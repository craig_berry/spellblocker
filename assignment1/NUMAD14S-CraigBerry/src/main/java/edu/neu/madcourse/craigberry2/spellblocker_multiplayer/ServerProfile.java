package edu.neu.madcourse.craigberry2.spellblocker_multiplayer;

import android.os.AsyncTask;

import java.util.ArrayList;

import edu.neu.madcourse.craigberry2.spellblocker.HealthServer;

/**
 * Created by craig on 3/22/14.
 */
public class ServerProfile extends AsyncTask<Integer, String, String> {

    // Action Codes
    public final int INIT = 11, GET_WINS = 12, SET_WINS = 13, GET_LOSSES = 14, SET_LOSSES = 15,
            GET_WORDS = 16, SET_WORDS = 17, GET_PENDING_CHALLENGES = 18, ADD_PENDING_CHALLENGE = 19,
            REMOVE_CHALLENGE = 20;

    // Keys for Health Server retrieval
    private static String WINS_KEY = "neu.edu.madcourse.craigberry.spellblockermultiplayer.serverprofile.wins",
                          LOSSES_KEY = "neu.edu.madcourse.craigberry.spellblockermultiplayer.serverprofile.losses",
                          WORDS_KEY = "neu.edu.madcourse.craigberry.spellblockermultiplayer.serverprofile.words",
                          PENDING_LIST_KEY = "neu.edu.madcourse.craigberry.spellblockermultiplayer.serverprofile.pendinglist";

    // Data Vars
    public String meid;

    // Constructor
    public ServerProfile(String meid) {
        this.meid = meid;
        execute(INIT);
    }

    @Override
    protected String doInBackground(Integer... params) {
        int count = params.length;

        for (int i=0; i < count; i++)
            switch (params[i]) {
                case INIT: verifyKeys();
                    break;
                case GET_WINS   : return getWins();
                case GET_LOSSES : return getLosses();
                case GET_WORDS  : return getWords();
                default:
                    break;
            }
        return "Nothing to return";
    }


    //////////////////////////////////////////////////////////////////
    /// Get and Increment Words, Wins, Losses
    //////////////////////////////////////////////////////////////////

    private String getWords() {
        String result = HealthServer.getValue(getWordsKey(meid));
        if (!result.contains("Error")) {
            return result;
        }
        else {
            return HealthServer.storeValue(getWordsKey(meid), "0");
        }
    }

    private String incrementWords() {
        String wordsStr = getWords();
        int wordsInt = Integer.parseInt(wordsStr) + 1;
        return HealthServer.storeValue(getWordsKey(meid), wordsInt + "");
    }

    private String getWins() {
        String result = HealthServer.getValue(getWinsKey(meid));
        if (!result.contains("Error")) {
            return result;
        }
        else {
            return HealthServer.storeValue(getWinsKey(meid), "0");
        }
    }

    private String incrementWins() {
        String winsStr = getWins();
        int winsInt = Integer.parseInt(winsStr) + 1;
        return HealthServer.storeValue(getWinsKey(meid), winsInt + "");
    }

    private String getLosses() {
        String result = HealthServer.getValue(getLossesKey(meid));
        if (!result.contains("Error")) {
            return result;
        }
        else {
            return HealthServer.storeValue(getLossesKey(meid), "0");
        }
    }

    private String incrementLosses() {
        String lossesStr = getLosses();
        int lossesInt = Integer.parseInt(lossesStr) + 1;
        return HealthServer.storeValue(getLossesKey(meid), lossesInt + "");
    }

    /////////////////////////////////////////////////////
    // INIT
    /////////////////////////////////////////////////////

    private void verifyKeys() {
        for (String key : getKeyList(meid)) {
            checkAndCreateKey(key);
        }
    }

    // List of keys to verify are created before playing multiplayer
    private Iterable<? extends String> getKeyList(String meid) {

        ArrayList<String> arr = new ArrayList<String>();
        arr.add(getWinsKey(meid));
        arr.add(getLossesKey(meid));
        arr.add(getWordsKey(meid));
        arr.add(getPendingListKey(meid));

        return arr;
    }

    // Checks if server has a value for the given key, if not create a blank entry
    private void checkAndCreateKey(String key) {

        if (!HealthServer.containsKey(key)) {
            HealthServer.storeValue(key, "0");
        }
    }


    @Override
    protected void onPostExecute(String result) {
        //addText(result);
    }

    //////////////////////////////////////////////////////////////////////
    // GetKey Functions
    //////////////////////////////////////////////////////////////////////

    public String getWinsKey(String meid) {
        return WINS_KEY + meid;
    }

    public String getLossesKey(String meid) {
        return LOSSES_KEY + meid;
    }

    public String getWordsKey(String meid) {
        return WORDS_KEY + meid;
    }

    public String getPendingListKey(String meid) {
        return PENDING_LIST_KEY + meid;
    }
}
