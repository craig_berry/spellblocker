package edu.neu.madcourse.craigberry2;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

public class TestDictionary extends ActionBarActivity {
    final int WORD_MAP_KEY_LENGTH = 3; // Number of letters wordMap uses for keys
    final int WORD_FILE_GRANULARITY = 2; // Number of letters the word files are stored as
    HashMap<String, ArrayList<String>> wordMap = new HashMap<String, ArrayList<String>>();
    ArrayList<String> foundWordsArr = new ArrayList<String>();
    boolean dictionaryLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_dictionary);

        // Setup
        setTitle("Test Dictionary");
        addUserInputTextListener();

        mp = MediaPlayer.create(this, R.raw.beep);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .commit();
        }
    }


    String previousFileLookup = "/";
    private void addUserInputTextListener() {
        EditText userInputEditText = (EditText) findViewById(R.id.dictTextBox);
        userInputEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String userInput = s.toString().toLowerCase();
                // If input reaches minimum search length and we haven't already found it
                if (userInput.length() >= WORD_FILE_GRANULARITY && !foundWordsArr.contains(userInput)) {
                    String fileLookupKey = userInput.substring(0, WORD_FILE_GRANULARITY);
                    // Load the dictionary if it isn't already loaded, or if we must change word files
                    if (!dictionaryLoaded || !previousFileLookup.equals(fileLookupKey)) {
                        cleanDictionary();
                        loadDictionary(fileLookupKey);
                    }
                    // Search for words minimum length 3
                    if (userInput.length() > 2) {
                        searchForWord(userInput);
                        previousFileLookup = fileLookupKey;
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                updateFoundWordView();
            }
        });
    }

    private void updateFoundWordView() {
        TextView textView = (TextView) findViewById(R.id.dictTextArea);
        textView.setText(getFormattedFoundWordList());
    }

    private CharSequence getFormattedFoundWordList() {
        String list = "";

        for (String s : foundWordsArr) {
            list += "  " + s + '\n';
        }
        return list;
    }

    private void searchForWord(String userInput) {
        String wordKey = userInput.substring(0, WORD_MAP_KEY_LENGTH);
        if (wordMap.containsKey(wordKey)) {
            ArrayList<String> arr = wordMap.get(wordKey);
            if (arr.contains(userInput) && !foundWordsArr.contains(userInput)) {
                foundWordsArr.add(userInput);
                playFoundWordNotification();
            }
        }
    }

    MediaPlayer mp;
    private void playFoundWordNotification() {
        try {
            mp.start();
        } catch (Exception e) {

        }
    }

    public void cleanDictionary() {
        wordMap = null;
        wordMap = new HashMap<String, ArrayList<String>>();
        dictionaryLoaded = false;
    }

    private void loadDictionary(String letter) {
        try {
            wordMap = new HashMap<String, ArrayList<String>>();
            parseWordList(getWordReader(letter), letter);
        } catch (IOException e) {

        }
        dictionaryLoaded = true;
    }

    private BufferedReader getWordReader(String inputString) {
        InputStream inputStream = getResources().openRawResource(getRawWordList(inputString));
        return new BufferedReader(new InputStreamReader(inputStream));
    }

    private void parseWordList(BufferedReader wordReader, String inputString) throws IOException {
        String currentLine;
        currentKey = "_INIT";
        sameKeyWordsArr = new ArrayList<String>();

        while ((currentLine = wordReader.readLine()) != null) {
            processWord(inputString, currentLine.toLowerCase().trim());
        }
        setWordMapBin(currentKey, sameKeyWordsArr);
    }

    private void processWord(String inputString, String currentWord) {
        String wordKey = currentWord.substring(0, WORD_MAP_KEY_LENGTH);

        if (matchedWordFile(inputString, wordKey)) {
            processMatchedWord(currentWord);
        }
    }

    String currentKey;
    ArrayList<String> sameKeyWordsArr = new ArrayList<String>();
    private void processMatchedWord(String currentWord) {
        String wordKey = currentWord.substring(0, WORD_MAP_KEY_LENGTH);

        if (!firstPass(currentKey) && wordKeyHasChanged(wordKey, currentKey)) {
            setWordMapBin(currentKey, sameKeyWordsArr);
            sameKeyWordsArr = new ArrayList<String>();
        }

        // Only deal with words with minimum 3 letters
        if (currentWord.length() > 2) {
            sameKeyWordsArr.add(currentWord);
            currentKey = wordKey;
        }
    }

    private void setWordMapBin(String currentKey, ArrayList<String> sameKeyWordsArr) {
        ArrayList<String> finalArr = new ArrayList<String>();
        finalArr.addAll(sameKeyWordsArr);
        wordMap.put(currentKey, finalArr);
    }

    private boolean wordKeyHasChanged(String wordKey, String currentKey) {
        return !currentKey.equals(wordKey);
    }

    private boolean firstPass(String currentKey) {
        return currentKey.equals("_INIT");
    }

    private boolean matchedWordFile(String inputString, String wordKey) {
        return wordKey.substring(0, WORD_FILE_GRANULARITY).equals(inputString);
    }

    // Clear Button
    public void dictClearAll(View view) {
        clearInputBox();
        clearFoundWords();
        cleanDictionary();
    }

    // Clears the inputted text
    private void clearInputBox() {
        EditText inputEditText = (EditText) findViewById(R.id.dictTextBox);
        inputEditText.setText("");
    }

    // Clears the found words textView and ArrayList
    private void clearFoundWords() {
        TextView foundWordsTextView = (TextView) findViewById(R.id.dictTextArea);
        foundWordsTextView.setText("");
        foundWordsArr = null;
        foundWordsArr = new ArrayList<String>();
    }

    // OnClick Methods
    ////////////////////

    public void startAcknowledgments (View view) {
        Intent startAckIntent = new Intent(getBaseContext(), TestDictionaryAcknowledgments.class);
        startActivity(startAckIntent);
    }

    public void returnToMainMenu(View view) {
        Intent mainMenuIntent = new Intent(getBaseContext(), MainScreen.class);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private int getRawWordList(String letter) {

        try {
            if (letter.equals("do") || letter.equals("if")) {
                letter += "2";
            }
            Field idField = R.raw.class.getDeclaredField(letter);
            return idField.getInt(idField);
        }
        catch (Exception e) {

        }
        return R.raw.zi;
    }


    /**
     * A placeholder fragment containing a simple view.

    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
    private void test() {
        long start = SystemClock.elapsedRealtime();
        loadDictionary("a");
        long end = SystemClock.elapsedRealtime();
        long over = end-start;
        int average = 0;
        for (int i = 0; i < 10; i++) {
        updateFoundWordView();
        start = SystemClock.elapsedRealtime();
        loadDictionary("a");
        end = SystemClock.elapsedRealtime();
        over = end - start;
        //foundWordsArr.add("Round A" + i + ":" + over);
        average += over;
        }
        //  foundWordsArr.clear();
        foundWordsArr.add("Average A:" + average/10);
        foundWordsArr.add(wordMap.size() + "");
        updateFoundWordView();
        average = 0;

        WORD_MAP_KEY_LENGTH++;
        for (int i = 0; i < 10; i++) {
        updateFoundWordView();
        start = SystemClock.elapsedRealtime();
        loadDictionary("a");
        end = SystemClock.elapsedRealtime();
        over = end - start;
        //  foundWordsArr.add("Round B" + i + ":" + over);
        average += over;
        }
        //foundWordsArr.clear();
        foundWordsArr.add("Average B:" + average/10);
        foundWordsArr.add(wordMap.size()+"");
        updateFoundWordView();

        }

     */

}
