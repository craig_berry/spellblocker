package edu.neu.madcourse.craigberry2;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import edu.neu.madcourse.craigberry2.communications.Communications;
import edu.neu.madcourse.craigberry2.spellblocker.SpellBlocker;
import edu.neu.madcourse.craigberry2.spellblocker_multiplayer.SpellblockerMultiplayer;

public class MainScreen extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_screen);
        setTitle(R.string.personal_name);
        setupButtons();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                   // .add(R.id.action_bar_container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //int id = item.getItemId();
        //if (id == R.id.action_settings) {
        //    return true;
        //}
        return super.onOptionsItemSelected(item);
    }

    private void setupButtons() {
        setupErrorButton();
        setupExitButton();
    }

    private void setupExitButton() {
        final Button exitBtn = (Button) findViewById(R.id.exitBtn);
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            // Wrap up and exit
            finish();
            System.exit(0);
            }
        });
    }

    private void setupErrorButton() {
        final Button errorBtn = (Button) findViewById(R.id.errorBtn);
        errorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // We need to generate any java error
                String[] strArr = new String[1];
                System.out.println(strArr[5]);
            }
        });
    }

    public void startAboutActivity(View view) {
        Intent intent = new Intent(view.getContext(), AboutActivity.class);
        startActivity(intent);
    }

    public void startSudokuActivity(View view) {
        Intent sudokuIntent = new Intent(getBaseContext(), Sudoku.class);
        startActivity(sudokuIntent);
    }

    public void startDictionaryActivity(View view) {
        Intent testDictIntent = new Intent(getBaseContext(), TestDictionary.class);
        startActivity(testDictIntent);
    }

    public void startWordGameActivity(View view) {
        Intent wordGameIntent = new Intent(getBaseContext(), SpellBlocker.class);
        startActivity(wordGameIntent);
    }

    public void startCommunicationsActivity(View view) {
        Intent commIntent = new Intent(getBaseContext(), Communications.class);
        startActivity(commIntent);
    }

    public void startTwoPlayerGame(View view) {
        Intent twoPlayerIntent = new Intent(getBaseContext(), SpellblockerMultiplayer.class);
        startActivity(twoPlayerIntent);
    }
}
