package edu.neu.madcourse.craigberry2.spellblocker;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import edu.neu.madcourse.craigberry2.R;

public class PauseActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spellblocker_pause);
        setTitle("Paused");
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .commit();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void resumeButtonClick(View view) {
        setResult(Game.RESUME_FROM_PAUSE);
        finish();
    }

    public void sbPauseExitBtnClick(View view) {
        setResult(Game.EXIT_RESULT_CODE);
        finish();
    }

    public void pauseNewGameClick(View view) {
        setResult(Game.NEW_GAME_FROM_PAUSE);
        finish();
    }

    public void hintBtnClick(View view) {
        setResult(Game.HINT_FROM_PAUSE);
        finish();
    }
}
