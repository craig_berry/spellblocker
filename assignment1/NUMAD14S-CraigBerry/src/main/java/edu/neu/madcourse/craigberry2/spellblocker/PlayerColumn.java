package edu.neu.madcourse.craigberry2.spellblocker;

/**
 * Created by craig on 2/21/14.
 */
public enum PlayerColumn {
    MEID(0),
    NAME(1),
    WINS_BATTLE(2),
    LOSSES_BATTLE(3),
    WINS_CHALLENGE(4),
    LOSSES_CHALLENGE(5),
    NUM_WORDS(6);

    private final int column;

    PlayerColumn(int column) {
        this.column = column;
    }

    public int val() {
        return column;
    }

    public static int totalColumns() {
        int total = 0;
        for (PlayerColumn pc : PlayerColumn.values()) {
            total = Math.max(total, pc.val());
        }
        return total + 1;
    }
}
