package edu.neu.madcourse.craigberry2.spellblocker;

/**
 * Created by craig on 2/21/14.
 */
public class Player {
    private String meid, name;
    private int winsBattle, lossesBattle, winsChallenge, lossesChallenge, wordsEntered;

    public Player(String meid) {
        this.meid = meid;
        this.name = "Player";
        this.winsBattle = 0;
        this.lossesBattle = 0;
        this.winsChallenge = 0;
        this.lossesChallenge = 0;
        this.wordsEntered = 0;
    }

    public String serialize(String delimeter) {
        String[] arr = new String[PlayerColumn.totalColumns()];
        arr[PlayerColumn.MEID.val()] = getMeid();
        arr[PlayerColumn.NAME.val()] = getName();
        arr[PlayerColumn.WINS_BATTLE.val()] = getWinsBattle() + "";
        arr[PlayerColumn.LOSSES_BATTLE.val()] = getLossesBattle() + "";
        arr[PlayerColumn.WINS_CHALLENGE.val()] = getWinsChallenge() + "";
        arr[PlayerColumn.LOSSES_CHALLENGE.val()] = getLossesChallenge() + "";
        arr[PlayerColumn.NUM_WORDS.val()] = getWordsEntered() + "";

        return buildString(arr, delimeter);
    }

    public static Player deserialize(String playerStr, String delimeter) {
        // Parse string to individual values
        String[] playerArr = playerStr.split(delimeter);
        // Create player object
        Player player = new Player(playerArr[PlayerColumn.MEID.val()]);

        // Parse String/int attributes
        player.setName(playerArr[PlayerColumn.NAME.val()]);
        player.setWinsBattle(Integer.parseInt(playerArr[PlayerColumn.WINS_BATTLE.val()]));
        player.setLossesBattle(Integer.parseInt(playerArr[PlayerColumn.LOSSES_BATTLE.val()]));
        player.setWinsChallenge(Integer.parseInt(playerArr[PlayerColumn.WINS_CHALLENGE.val()]));
        player.setLossesChallenge(Integer.parseInt(playerArr[PlayerColumn.LOSSES_CHALLENGE.val()]));
        player.setWordsEntered(Integer.parseInt(playerArr[PlayerColumn.NUM_WORDS.val()]));

        // Return the player
        return player;
    }

    private String buildString(String[] arr, String delimeter) {
        String result = "";

        for (String s : arr) {
            result += s + delimeter;
        }
        return result;
    }

    /*
        * Getters and Setters
        */
    public String getMeid() {
        return meid;
    }

    public void setMeid(String meid) {
        this.meid = meid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getWinsBattle() {
        return winsBattle;
    }

    public void setWinsBattle(int winsBattle) {
        this.winsBattle = winsBattle;
    }

    public int getLossesBattle() {
        return lossesBattle;
    }

    public void setLossesBattle(int lossesBattle) {
        this.lossesBattle = lossesBattle;
    }

    public int getWinsChallenge() {
        return winsChallenge;
    }

    public void setWinsChallenge(int winsChallenge) {
        this.winsChallenge = winsChallenge;
    }

    public int getLossesChallenge() {
        return lossesChallenge;
    }

    public void setLossesChallenge(int lossesChallenge) {
        this.lossesChallenge = lossesChallenge;
    }

    public int getWordsEntered() {
        return wordsEntered;
    }

    public void setWordsEntered(int wordsEntered) {
        this.wordsEntered = wordsEntered;
    }
}
