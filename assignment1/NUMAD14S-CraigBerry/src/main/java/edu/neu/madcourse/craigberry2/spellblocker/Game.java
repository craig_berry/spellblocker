/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.craigberry2.spellblocker;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import edu.neu.madcourse.craigberry2.R;
import edu.neu.madcourse.craigberry2.SudokuPrefs;
import edu.neu.madcourse.craigberry2.spellblocker_multiplayer.ChallengeMode;

public class Game extends Activity {

    // Intent keys
    public static final String DIFF_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.difficulty";
    public static final String SCORE_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.score";
    public static final String TIME_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.time";
    public static final String BLOCK_TOTAL_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.blockstotal";
    public static final String BLOCK_LEFT_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.blocksleft";
    public static final String WON_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.won";
    public static final String MEID_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.meid";
    public static final String WORD_NUM_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.wordnum";

    private static final String PUZZLE = "edu.neu.madcourse.craigberry.spellblocker.puzzle";
    private static final String COLOR_MAP = "edu.neu.madcourse.craigberry.spellblocker.colormap";
    private static final String HILITE_BLKS = "neu.edu.neu.madcourse.craigberry.spellblocker.hiliteblocks";
    private static final String LETTERS = "neu.edu.madcourse.craigberry.spellblocker.letters";
    private static final String WORD_COUNT = "neu.edu.madcourse.craigberry.spellblocker.wordcount";
    private static final String CUR_WORD = "neu.edu.madcourse.craigberry.spellblocker.currentword";

    public static final String PREF_KEY = "edu.neu.madcourse.craigberry.spellblocker.prefs";
    public static final String PREF_CONT = "edu.neu.madcourse.craigberry.spellblocker.continue";
    SharedPreferences prefs;

    // Result/Request codes for talking to other activities
    public static final int EXIT_RESULT_CODE = 99;
    public static final int HINT_FROM_PAUSE = 98;
    public static final int NEW_GAME_FROM_PAUSE = 97;
    public static final int RESUME_FROM_PAUSE = 96;
    public static final int PAUSE_REQUEST_CODE = 68;
    public static final int ENDGAME_REQUEST_CODE = 67;

    // Difficulty codes
    public static final int DIFFICULTY_EASY = 0;
    public static final int DIFFICULTY_MEDIUM = 1;
    public static final int DIFFICULTY_HARD = 2;
    protected static final int DIFFICULTY_CONTINUE = -1;

    // Preferences
    private static final String TAG = "SpellBlocker";
    private static final String PREF_GRID = "grid";
    private static final String PREF_HIGHLIGHT = "highlights";
    private static final String PREF_SELECTED = "selecteds";
    private static final String PREF_COLORS = "colors";
    private static final String PREF_DIFFICULTY = "difficulty";

    // Alphabets for random letters. Difficulties correspond to letter frequencies
    private final String EASY_ALPHABET =
            "aaaaaaaabbcccddddeeeeeeeeeeeeeffgghhhhhhiiiiiiijkkllllmmnnnnnnooooooooppqrrrrrrsssssstttttttttuuuvvwwwxyyyz";
    private final String MEDIUM_ALPHABET =
            "aaaaabbbbccbccddddeeeeeffffgggghhhhiiiijjjkkkkllllllmmmmnnnnnooooooppppqqrrrrrrssssstttttttttuuuvvvwwwxxxyyyzzz";
    private final String HARD_ALPHABET =
            "abcdefghijklmnopqrstuvwxyz";
    private final String testWordPuzzle =
            "e p s p o g b u w i e j t k s v k u a w g k u d u f i t q o r l e a f";

    // State booleans
    public boolean submittedWord = false;
    private boolean wasPaused = false;
    protected boolean showHint = false;
    private boolean gameRunning = false;
    private boolean isChallenge = false;

    // Default Game settings
    protected String alphabet = EASY_ALPHABET;
    protected int PUZZLE_HEIGHT = 7;
    protected int PUZZLE_WIDTH = 10;
    protected int MINIMUM_LINE_LENGTH = 3;
    protected int SCORE_MULTIPLIER = 100;
    protected int WORD_LENGTH_MULTIPLIER_MINIMUM = 4;
    private final int STARTING_TIME = 100;
    private final int BLOCKS_TO_CLEAR = 20;

    // Game variables
    protected Point previousSelection = new Point(-1, -1);
    private ArrayList<Point> selectedSquares;
    private ArrayList<Point> highlightedSquares;
    private HashMap<Point, Integer> squareColors;
    private String puzzle[];
    private WordSearch wordSearch;
    private int time = STARTING_TIME;
    private Timer timer = new Timer();
    public Point hint = new Point(-1,-1);
    private int score = 0;
    private int difficulty = 1;
    private int wordsEntered;
    private int totalBlocks = BLOCKS_TO_CLEAR;
    private int remainingBlocks = totalBlocks-17;
    private String currentWord = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        // Main Layout
        setContentView(R.layout.spellblocker);
        addUserInputTextListener();

        // Set continue game flag
        prefs = this.getSharedPreferences(PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(PREF_CONT, true).commit();

        // Get difficulty/continue from main screen
        int diff = this.getIntent().getIntExtra(DIFF_EXTRA, DIFFICULTY_EASY);
        isChallenge = this.getIntent().getBooleanExtra(ChallengeMode.IS_CHALLENGE_EXTRA, false);

        // Initialize game
        init(diff);

        // Init if not a continue, unaffected by prefs above, decided by difficulty intent passed in
        if (diff == DIFFICULTY_CONTINUE) {
            loadPreferences();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (wasPaused) {
            startTimer();
            setTime(pauseTime);
            wasPaused = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        wasPaused = true;
        pauseTime = getTime();
        stopTimer();
        Log.d(TAG, "onPause");
        savePreferences();
    }


    @Override
    public void finish() {
        Intent data = new Intent();

        if (isChallenge) {
            if (isWin) {
                data.putExtra(ChallengeMode.SCORE_EXTRA, getScore());
                data.putExtra(ChallengeMode.TIME_EXTRA, getTime());
                data.putExtra(ChallengeMode.WORD_EXTRA, getTotalEnteredWords());
                setResult(ChallengeMode.GAME_WON_RESULT, data);
            }
            else {
                setResult(ChallengeMode.GAME_LOST_RESULT);
            }
        }

        super.finish();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void loadPreferences() {
        setCurrentWord(prefs.getString(CUR_WORD, ""));
        setPuzzle(stringToPuzzle(prefs.getString(PUZZLE, puzzleToString(generatePuzzle()))));
        setScore(prefs.getInt(SCORE_EXTRA, 0));
        setTime(prefs.getInt(TIME_EXTRA, STARTING_TIME));
        setRemainingBlocks(prefs.getInt(BLOCK_LEFT_EXTRA, BLOCKS_TO_CLEAR));
        setTotalBlocks(prefs.getInt(BLOCK_TOTAL_EXTRA, BLOCKS_TO_CLEAR));
        setTotalEnteredWords(prefs.getInt(WORD_COUNT, 0));
        setSquareColors(setToHashMap(prefs.getStringSet(COLOR_MAP, new HashSet<String>())));
        setHighlightedSquares(setToArrayList(prefs.getStringSet(HILITE_BLKS, new HashSet<String>())));
        setSelectedSquares(setToArrayList(prefs.getStringSet(LETTERS, new HashSet<String>())));
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private void savePreferences() {
        prefs.edit().putString(CUR_WORD, getCurrentWord())
                    .putString(PUZZLE, puzzleToString(getPuzzle()))
                    .putInt(SCORE_EXTRA, getScore())
                    .putInt(TIME_EXTRA, getTime())
                    .putInt(BLOCK_LEFT_EXTRA, getRemainingBlocks())
                    .putInt(BLOCK_TOTAL_EXTRA, getTotalBlocks())
                    .putInt(WORD_COUNT, getTotalEnteredWords())
                    .putStringSet(COLOR_MAP, parseColorPref(getSquareColors()))
                    .putStringSet(HILITE_BLKS, parsePoints(getHighlightedList()))
                    .putStringSet(LETTERS, parsePoints(getSelectedList()))
                .commit();
    }

    private String getMEID() {
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    private Set<String> parseColorPref(HashMap<Point, Integer> squareColors) {
        HashSet<String> set = new HashSet<String>();

        for (Point point : squareColors.keySet()) {
            set.add(point.x + " " + point.y + " " + squareColors.get(point));
        }

        return set;
    }

    private HashMap<Point, Integer> setToHashMap(Set<String> set) {
        HashMap<Point, Integer> map = new HashMap<Point, Integer>();

        for (String entry : set) {
            String[] entryArr = entry.split(" ");
            Point keyPoint = new Point(Integer.parseInt(entryArr[0]), Integer.parseInt(entryArr[1]));
            map.put(keyPoint, Integer.parseInt(entryArr[2]));
        }

        return map;
    }

    private Set<String> parsePoints(ArrayList<Point> points) {
        HashSet<String> set = new HashSet<String>();

        for (Point point : points) {
            set.add(point.x + " " + point.y);
        }
        return set;
    }

    private ArrayList<Point> setToArrayList(Set<String> set) {
        ArrayList<Point> arr = new ArrayList<Point>();

        for (String entry : set) {
            String[] entryArr = entry.split(" ");
            arr.add(new Point(Integer.parseInt(entryArr[0]), Integer.parseInt(entryArr[1])));
        }

        return arr;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.spellblocker_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.settings:
                startActivity(new Intent(this, SudokuPrefs.class));
                return true;
            // More items go here (if any) ...
        }
        return false;
    }

    private void init(int diff) {
        difficulty = diff;
        selectedSquares = new ArrayList<Point>();
        highlightedSquares = new ArrayList<Point>();
        squareColors = new HashMap<Point, Integer>();
        wordsEntered = 0;
        wordSearch = new WordSearch(getResources());
        puzzle = getPuzzle(diff);
        time = STARTING_TIME;
        if (diff != DIFFICULTY_CONTINUE) {
            Music.playUnpause(this, R.raw.sb_unpause);
        }
        updateBlockText();
        updateTimeText();
        updateScoreText();
        startTimer();
    }

    private void startTimer() {
        stopTimer();
        timer = new Timer();
        setGameRunning(true);
        GameTimer timerTask = new GameTimer(this);
        timer.schedule(timerTask, 1000, 1000);
    }

    private void invalidateBoard() {
        GraphicsView graphicsView = (GraphicsView) findViewById(R.id.wgGraphicsView);
        graphicsView.invalidate();
    }

    void stopTimer() {
        setGameRunning(false);
        timer.cancel();
        timer.purge();
    }

    /**
     * Convert an array into a puzzle string
     */
    static private String puzzleToString(String[] puz) {
        StringBuilder buf = new StringBuilder();
        for (String element : puz) {
            buf.append(element);
        }
        return buf.toString();
    }

    private String[] stringToPuzzle(String letters) {
        String[] arr = new String[letters.length()];

        for (int i = 0; i < PUZZLE_HEIGHT * PUZZLE_WIDTH; i++) {
            arr[i] = letters.substring(i, i+1);
        }
        return arr;
    }

    /**
     * Given a difficulty level, come up with a new puzzle
     */
    private String[] getPuzzle(int difficulty) {
        switch (difficulty) {
            case DIFFICULTY_CONTINUE:
                if (getPreferences(MODE_PRIVATE).getString(PREF_GRID, puzzleToString(generatePuzzle())).length() == PUZZLE_HEIGHT * PUZZLE_HEIGHT) {
                    return stringToPuzzle(getPreferences(MODE_PRIVATE).getString(PREF_GRID, puzzleToString(generatePuzzle())));
                }
                break;
            case DIFFICULTY_EASY: setAlphabet(EASY_ALPHABET);
                break;
            case DIFFICULTY_MEDIUM: setAlphabet(MEDIUM_ALPHABET);
                break;
            case DIFFICULTY_HARD: setAlphabet(HARD_ALPHABET);
                break;
            default:
                break;
        }

        return generatePuzzle();
    }

    private String[] generatePuzzle() {
        String[] arr = new String[PUZZLE_WIDTH * PUZZLE_HEIGHT];

        for (int i = 0; i < PUZZLE_HEIGHT * PUZZLE_WIDTH; i++) {
            arr[i] = getRandomLetter();
        }
        return arr;
    }

    /**
     * Return the tile at the given coordinates
     */
    private String getTile(int x, int y) {
        return puzzle[y * PUZZLE_WIDTH + x];
    }

    /**
     * Change the tile at the given coordinates
     */
    private void setTile(int x, int y, String value) {
        puzzle[y * PUZZLE_WIDTH + x] = value;
    }

    /**
     * Return a string for the tile at the given coordinates
     */
    protected String getTileString(Point point) {
        String v = getTile(point.x, point.y);
        if (v == "")
            return "";
        else
            return v;
    }

    public void addSelectedSquare(Point point, int color) {
        if (!selectedSquares.contains(point)) {
            selectedSquares.add(point);

            if (squareColors.containsKey(point)) {
                squareColors.remove(point);
            }

            if (color != Color.BLACK) {
                squareColors.put(point, color);
            } else {
                squareColors.put(point, getRandomSquareColor(5));
            }
        }
    }

    private int getRandomSquareColor(int seed) {
        Random r = new Random();
        seed = 6;
        switch (r.nextInt(seed)) {
            case 0:
                return Color.BLUE;
            case 1:
                return Color.RED;
            case 2:
                return Color.YELLOW;
            case 3:
                return Color.MAGENTA;
            case 4:
                return Color.GREEN;
            case 5:
                return Color.CYAN;
            default:
                return -1;
        }
    }

    public void addHighlightedSquare(Point point) {
        if (!highlightedSquares.contains(point)) {
            highlightedSquares.add(point);
        }
    }

    public void submitButtonClick(View view) {
        if (wordSearch.isWord(getCurrentWord())) {
            // This if processes the correct word, and uses the return val to avoid playing two sounds
            if (!processCorrectWord(getCurrentWord())) {
                playCorrectSound();
            }
        } else {
            clearInputBox();
            updateBoard(false);
        }
        updateGraphicsAfterSubmit();
    }

    private void playCorrectSound() {
        Integer[] sounds = new Integer[2];
        sounds[0] = R.raw.sb_correct1;
        sounds[1] = R.raw.sb_correct2;
        Music.playCorrect(this, sounds);
    }

    private boolean processCorrectWord(CharSequence text) {
        updateScore(text.toString());
        updateBoard(true);

        // Puzzler element to game
        return clearSetsOfSquares();

    }

    private boolean clearSetsOfSquares() {
        return removeHighlightedLine(removeDuplicates(getSquareRemoveList()));
    }

    private ArrayList<Point> getSquareRemoveList() {
        ArrayList<Point> arr = (ArrayList<Point>) highlightedSquares.clone();
        ArrayList<Point> removeList = new ArrayList<Point>();
        ArrayList<Point> removeListTotal = new ArrayList<Point>();
        for (Point p : arr) {
            removeList.addAll(checkForLine(p, 1, 0, true, true));
            removeList.addAll(checkForLine(p, 1, 1, true, true));
            removeList.addAll(checkForLine(p, 0, 1, true, true));
            removeList.addAll(checkForLine(p, -1, 1, true, true));

            if (removeList.size() >= (MINIMUM_LINE_LENGTH - 1)) {
                removeList.add(p);
            } else {
                removeList.clear();
            }
            removeListTotal.addAll(removeList);
            removeList.clear();
        }
        if (removeListTotal.size() > MINIMUM_LINE_LENGTH + 1) {
            //
        }
        return removeListTotal;
    }

    private ArrayList<Point> removeDuplicates(ArrayList<Point> array) {
        ArrayList<Point> distinctPoints = new ArrayList<Point>();
        for (Point point : array) {
            if (!distinctPoints.contains(point)) {
                distinctPoints.add(point);
            }
        }
        return distinctPoints;
    }

    // Search for line of highlighted squares in the given (xAdd, yAdd) direction
    private ArrayList<Point> checkForLine(Point firstPoint, int xAdd, int yAdd, boolean checkInverse, boolean checkColor) {
        ArrayList<Point> removeList = new ArrayList<Point>();
        Point secPoint = new Point(firstPoint.x + xAdd, firstPoint.y + yAdd);

        // If we hit in suspected direction
        if (checkHighlightedSquares(secPoint, checkColor, getSquareColor(firstPoint))) {
            removeList.add(secPoint);
            removeList.addAll(loopCurrentDirection(secPoint, xAdd, yAdd, checkColor, getSquareColor(firstPoint)));

            // Switch to check inverse direction
            if (checkInverse) {
                removeList.addAll(loopCurrentDirection(firstPoint, -1 * xAdd, -1 * yAdd, checkColor, getSquareColor(firstPoint)));
            }
        }

        // If we hit minimum length remove the blocks
        if (removeList.size() < (MINIMUM_LINE_LENGTH - 1)) {
            removeList.clear();
        }
        return removeList;
    }

    private boolean checkHighlightedSquares(Point point, boolean checkColor, int color) {
        if (inBounds(point) && highlightedSquares.contains(point)) {
            if (checkColor) {
                return color == getSquareColor(point);
            }
            return true;
        }
        return false;
    }

    private ArrayList<Point> loopCurrentDirection(Point currentPoint, int xAdd, int yAdd, boolean checkColor, int color) {
        boolean run = true;
        ArrayList<Point> removeList = new ArrayList<Point>();

        while (run) {
            currentPoint = new Point(currentPoint.x + xAdd, currentPoint.y + yAdd);
            if (checkHighlightedSquares(currentPoint, checkColor, color)) {
                removeList.add(currentPoint);
            } else {
                run = false;
            }
        }
        return removeList;
    }

    // Check whether given point corresponds to a valid square in the grid
    public boolean inBounds(Point point) {
        int x = point.x,
                y = point.y;
        return x < PUZZLE_WIDTH && x >= 0 && y < PUZZLE_HEIGHT && y >= 0;
    }

    // Remove all blocks from remove list
    private boolean removeHighlightedLine(ArrayList<Point> removeList) {
        boolean clearedAnything = false;
        for (Point pointToRemove : removeList) {
            highlightedSquares.remove(pointToRemove);
            setTile(pointToRemove.x, pointToRemove.y, getRandomLetter());
        }
        setRemainingBlocks(remainingBlocks - removeList.size());

        if (removeList.size() > MINIMUM_LINE_LENGTH-1) {
            clearedAnything = true;
            Music.playBigClear(this, R.raw.sb_big_clear);
        }
        // Update TextView
        updateBlockText();
        return clearedAnything;
    }

    private String getRandomLetter() {
        Random r = new Random();
        return alphabet.charAt(r.nextInt(alphabet.length())) + "";
    }

    public void updateDoubleTap(Point currentSelection) {
        // If previous selection was a highlighted square
        if (highlightedSquares.contains(previousSelection)) {
            // .. and new selection is adjacent to previous
            if (Math.abs(currentSelection.x - previousSelection.x) <= 1 &&
                    Math.abs(currentSelection.y - previousSelection.y) <= 1) {
                simulateMove(previousSelection, currentSelection);
                invalidateBoard();
            }
        }
    }

    // Function that checks if the move of previous highlighted square to adjacent current selection
    // will create a valid line. It executes this action if it does, penalizes the player if it does not
    private void simulateMove(Point previousSelection, Point currentSelection) {
        if (!previousSelection.equals(currentSelection)) {
            // Initialize
            ArrayList<Point> previousHighlights = (ArrayList<Point>) highlightedSquares.clone();
            HashMap<Point, Integer> previousColors = (HashMap<Point, Integer>) squareColors.clone();
            String currentTile = getTile(currentSelection.x, currentSelection.y);

            // Simulate switch of squares
            if (!isHighlighted(currentSelection)) {
                highlightedSquares.remove(previousSelection);
                highlightedSquares.add(currentSelection);
                squareColors.put(currentSelection, squareColors.get(previousSelection));
                squareColors.remove(previousSelection);

            } else {
                int tempColor = getSquareColor(previousSelection);
                setSquareColor(previousSelection, getSquareColor(currentSelection));
                setSquareColor(currentSelection, tempColor);
            }

            // Run the sim
            ArrayList<Point> removeListTotal = getSquareRemoveList();

            // If the simulated move creates a new line
            if (removeListTotal.size() >= MINIMUM_LINE_LENGTH) {
                // It is confirmed and the letters swapped!
                removeHighlightedLine(removeDuplicates(removeListTotal));
                setTile(previousSelection.x, previousSelection.y, currentTile);
                setSquareColor(currentSelection, getRandomSquareColor(4));
            }
            else {
                GraphicsView g = (GraphicsView) findViewById(R.id.wgGraphicsView);
                highlightedSquares = previousHighlights;
                squareColors = previousColors;
                Music.playFailed(this, R.raw.sb_failed_move);
            }
        }

    }

    private void updateGraphicsAfterSubmit() {
        // Clear input refresh canvas
        clearInputBox();
        invalidateBoard();
    }

    //
    private void updateBoard(boolean correctWordSubmitted) {
        if (correctWordSubmitted) {
            setTotalEnteredWords(wordsEntered + 1);
            highlightedSquares.addAll((ArrayList<Point>) selectedSquares.clone());
            submittedWord = true;
        }
        selectedSquares.clear();
    }

    // Add scored word to score
    private void updateScore(String word) {
        // Multiply scrabble word score my multiplier
        int wordScore = (getWordScore(word) * SCORE_MULTIPLIER);
        // Multiply word score by number of letters over min
        if (word.length() > WORD_LENGTH_MULTIPLIER_MINIMUM) {
            wordScore = wordScore * (word.length() - (WORD_LENGTH_MULTIPLIER_MINIMUM - 1));
        }
        // Add to total score
        setScore(score + wordScore);
        updateScoreText();
    }

    private void updateBlockText(){
        TextView blockText = (TextView) findViewById(R.id.sbBlockText);
        blockText.setText(getRemainingBlocks() + "/" + getTotalBlocks());
    }

    private void updateTimeText() {
        TextView timeText = (TextView) findViewById(R.id.sbTimeText);
        if (getTime() < 10) {
            timeText.setTextColor(Color.RED);
        }
        else {
            timeText.setTextColor(Color.BLACK);
        }
        timeText.setText(getTime() + "");
    }

    private void updateScoreText() {
        TextView scoreText = (TextView) findViewById(R.id.sbScoreText);
        if (getScore() < 0) {
            scoreText.setTextColor(Color.RED);
        }
        else {
            scoreText.setTextColor(Color.BLACK);
        }
        scoreText.setText(getScore() + "");
    }

    // Uses scrabble scoring
    private int getWordScore(String word) {
        int wordScore = 0;
        if (word.contains("a")) {
            wordScore += 1;
        }
        if (word.contains("b")) {
            wordScore += 3;
        }
        if (word.contains("c")) {
            wordScore += 3;
        }
        if (word.contains("d")) {
            wordScore += 2;
        }
        if (word.contains("e")) {
            wordScore += 1;
        }
        if (word.contains("f")) {
            wordScore += 4;
        }
        if (word.contains("g")) {
            wordScore += 2;
        }
        if (word.contains("h")) {
            wordScore += 4;
        }
        if (word.contains("i")) {
            wordScore += 1;
        }
        if (word.contains("j")) {
            wordScore += 8;
        }
        if (word.contains("k")) {
            wordScore += 5;
        }
        if (word.contains("l")) {
            wordScore += 1;
        }
        if (word.contains("m")) {
            wordScore += 3;
        }
        if (word.contains("n")) {
            wordScore += 1;
        }
        if (word.contains("o")) {
            wordScore += 1;
        }
        if (word.contains("p")) {
            wordScore += 3;
        }
        if (word.contains("q")) {
            wordScore += 10;
        }
        if (word.contains("r")) {
            wordScore += 1;
        }
        if (word.contains("s")) {
            wordScore += 1;
        }
        if (word.contains("t")) {
            wordScore += 1;
        }
        if (word.contains("u")) {
            wordScore += 1;
        }
        if (word.contains("v")) {
            wordScore += 4;
        }
        if (word.contains("w")) {
            wordScore += 4;
        }
        if (word.contains("x")) {
            wordScore += 8;
        }
        if (word.contains("y")) {
            wordScore += 4;
        }
        if (word.contains("z")) {
            wordScore += 10;
        }
        return wordScore;
    }

    // Clear Button
    public void clearButtonClick(View view) {
        clearInputBox();
        updateBoard(false);
        updateGraphicsAfterSubmit();
    }

    // Clears the inputted text
    private void clearInputBox() {
        TextView inputTextView = (TextView) findViewById(R.id.wgInputTextView);
        inputTextView.setText("");
        setCurrentWord("");
    }

    // Adds TextWatcher to letter input box
    private void addUserInputTextListener() {
        TextView userInputEditText = (TextView) findViewById(R.id.wgInputTextView);
        userInputEditText.addTextChangedListener(getInputTextWatcher());
    }

    protected void playIncorrectSound() {
        Music.playIncorrect(this, R.raw.sb_wrong_letter);
    }

    // Checks that occur after a letter has been added or removed to the input box
    private TextWatcher getInputTextWatcher() {
        return new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                TextView userInputTextView = (TextView) findViewById(R.id.wgInputTextView);
                userInputTextView.setTextColor(Color.WHITE);
                currentWord = s.toString();

                if (wordSearch.isWord(s.toString())) {
                    userInputTextView.setTextColor(Color.GREEN);
                } else if (!wordSearch.containsKey(s.toString()) && s.length() > 3) {
                    GraphicsView graphicsView = (GraphicsView) findViewById(R.id.wgGraphicsView);
                    graphicsView.shakeBoardAnimation();
                    playIncorrectSound();
                    userInputTextView.setTextColor(Color.RED);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };
    }

    int pauseTime = 0;

    public void pauseButtonClick(View view) {
        pauseTime = getTime();
        Intent intent = new Intent(view.getContext(), PauseActivity.class);
        startActivityForResult(intent, PAUSE_REQUEST_CODE);
        Music.playPause(this, R.raw.sb_pause);
    }

    boolean isWin = false;

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("RESULT", resultCode + "");
        // Check request was from us
        if (requestCode == PAUSE_REQUEST_CODE || requestCode == ENDGAME_REQUEST_CODE) {
            if (resultCode == EXIT_RESULT_CODE) {
                isWin = data.getBooleanExtra(Game.WON_EXTRA, false);
                finish();
            }
            else if (resultCode == HINT_FROM_PAUSE) {
                processHint();
            }
            else if (resultCode == NEW_GAME_FROM_PAUSE) {
                stopTimer();
                init(difficulty);
                invalidateBoard();
            }
        }
    }

    private void processHint() {
        hint = getBestPossibleMove();
        if (inBounds(hint)) {
            showHint = true;
            setScore(getScore()/2);
            invalidateBoard();
        }
        else {
            showToast("No hints available");
        }
    }

    private Point getBestMove(HashMap<Integer,Point> moveMap) {
        int highest = 0;
        for (int clears : moveMap.keySet()) {
            highest = Math.max(highest, clears);
        }
        if (highest == 0) {
            return new Point (-1, -1);

        }
        return moveMap.get(highest);
    }

    public Point getBestPossibleMove() {
        HashMap<Integer, Point> moveMap = new HashMap<Integer, Point>();
        // For each square
        for (Point p: (ArrayList<Point>) highlightedSquares.clone()) {
            
            //Calculate the possible moves into the hashmap
            moveMap.put(hintSimulation( new Point(p.x, p.y + 1), p), new Point(p.x, p.y));
            moveMap.put(hintSimulation( new Point(p.x, p.y - 1), p), new Point(p.x, p.y));
            moveMap.put(hintSimulation( new Point(p.x + 1, p.y), p), new Point(p.x, p.y));
            moveMap.put(hintSimulation( new Point(p.x - 1, p.y), p), new Point(p.x, p.y));
            moveMap.put(hintSimulation( new Point(p.x + 1, p.y + 1), p), new Point(p.x, p.y));
            moveMap.put(hintSimulation( new Point(p.x + 1, p.y - 1), p), new Point(p.x, p.y));
            moveMap.put(hintSimulation( new Point(p.x - 1, p.y + 1), p), new Point(p.x, p.y));
            moveMap.put(hintSimulation( new Point(p.x - 1, p.y - 1), p), new Point(p.x, p.y));
        }
        return getBestMove(moveMap);
    }

    private int hintSimulation(Point currentSelection, Point previousSelection) {
        if (inBounds(currentSelection) && inBounds(previousSelection)) {
        // Initialize
            ArrayList<Point> previousHighlights = (ArrayList<Point>) highlightedSquares.clone();
            HashMap<Point, Integer> previousColors = (HashMap<Point, Integer>) squareColors.clone();
            String currentTile = getTile(currentSelection.x, currentSelection.y);

            // Simulate switch of squares
            if (!isHighlighted(currentSelection)) {
                highlightedSquares.remove(previousSelection);
                highlightedSquares.add(currentSelection);
                squareColors.put(currentSelection, squareColors.get(previousSelection));
                squareColors.remove(previousSelection);

            } else {
                int tempColor = getSquareColor(previousSelection);
                setSquareColor(previousSelection, getSquareColor(currentSelection));
                setSquareColor(currentSelection, tempColor);
            }
            // Run the sim
            int best =  getSquareRemoveList().size();
            highlightedSquares = (ArrayList<Point>) previousHighlights.clone();
            squareColors = (HashMap<Point, Integer>) previousColors.clone();

            return best;
        }
        return -1;
    }
    ////////////////////////////////////////////////////////////
    // GETTERS AND SETTERS
    ////////////////////////////////////////////////////////////

    public int getTotalBlocks() {
        return totalBlocks;
    }

    public void setTotalBlocks(int blocks) {
        if (blocks > 0) {
            totalBlocks = blocks;
            updateBlockText();
        }
    }

    public boolean isRunning() {
        return gameRunning;
    }

    public void setGameRunning(boolean gameRunning) {
        this.gameRunning = gameRunning;
    }

    public int getTotalEnteredWords() {
        return wordsEntered;
    }

    public void setTotalEnteredWords(int newNum) {
        wordsEntered = newNum;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public void setTime(int time) {
        if (time > 0) {
            this.time = time;
        }
        else {
            this.time = 0;
        }
    }

    public int getTime() {
        return time;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int newScore) {
        score = newScore;
        updateScoreText();
    }

    public int getRemainingBlocks() {
        return remainingBlocks;
    }

    public void setRemainingBlocks(int blocks) {
        if (getRemainingBlocks() != blocks) {
            if (blocks <= 0) {
                remainingBlocks = 0;
                Intent i = new Intent(getSelf(), EndGame.class);
                i.putExtra(WON_EXTRA, true);
                i.putExtra(SCORE_EXTRA, getScore());
                i.putExtra(TIME_EXTRA, STARTING_TIME - getTime());
                i.putExtra(WORD_NUM_EXTRA, getTotalEnteredWords());
                i.putExtra(MEID_EXTRA, "9327584235634892");
                startActivityForResult(i, ENDGAME_REQUEST_CODE);
            }
            else {
                int diff = getRemainingBlocks() - blocks;
                remainingBlocks = blocks;
                showToast(diff + " blocks cleared");
            }
            updateBlockText();
        }
    }

    public ArrayList<Point> getSelectedList() {
        return (ArrayList<Point>) selectedSquares.clone();
    }

    public void setSelectedSquares(ArrayList<Point> selectedSquares) {
        this.selectedSquares.clear();
        this.selectedSquares.addAll(selectedSquares);
    }

    public boolean isSelected(Point point) {
        return selectedSquares.contains(point);
    }

    public String getCurrentWord() {
        return currentWord;
    }

    public void setCurrentWord(String currentWord) {
        TextView t = (TextView) findViewById(R.id.wgInputTextView);
        t.setText(currentWord);
        this.currentWord = currentWord;
    }

    public ArrayList<Point> getHighlightedList() {
        return (ArrayList<Point>) highlightedSquares.clone();
    }

    public void setHighlightedSquares(ArrayList<Point> newHighlightedSquares) {
        this.highlightedSquares.clear();
        this.highlightedSquares.addAll(newHighlightedSquares);
    }

    public boolean isHighlighted(Point point) {
        if (highlightedSquares.size() > 0) {
            return highlightedSquares.contains(point);
        }
        else {
            return false;
        }
    }

    public HashMap<Point, Integer> getSquareColors() {
        return (HashMap<Point, Integer>) squareColors.clone();
    }

    public void setSquareColors(HashMap<Point, Integer> map) {
        squareColors.clear();
        squareColors = (HashMap<Point, Integer>) map.clone();
    }
    public int getSquareColor(Point point) {
        return squareColors.get(point);
    }

    public void setSquareColor(Point point, int color) {
        squareColors.put(point, color);
    }

    public String[] getPuzzle() {
        return puzzle;
    }

    public void setPuzzle(String[] puzzle) {
        this.puzzle = puzzle;
    }

    public void setAlphabet(String alphabet) {
        this.alphabet = alphabet;
    }

    //////////////////////////////////////////////////////////////////////////////
    //// Miscellaneous Functions
    //////////////////////////////////////////////////////////////////////////////

    public void showToast(String msg) {
        Context context = getApplicationContext();
        CharSequence text = msg;
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }

    public Game getSelf() {
        return this;
    }

    public class GameTimer extends TimerTask {
        Game game;

        GameTimer(Context context) {
            this.game = (Game) context;
        }

        @Override
        public void run() {
            if (!wasPaused) {
                setTime(getTime() - 1);
            }
            // Game over on time
            if (getTime() == 0) {
                Intent i = new Intent(getSelf(), EndGame.class);
                i.putExtra(WON_EXTRA, false);
                i.putExtra(SCORE_EXTRA, getScore());
                i.putExtra(BLOCK_LEFT_EXTRA, getRemainingBlocks());
                i.putExtra(BLOCK_TOTAL_EXTRA, getTotalBlocks());
                i.putExtra(DIFF_EXTRA, getDifficulty());
//                i.putExtra(MEID_EXTRA, getMEID());
                i.putExtra(MEID_EXTRA, "9327584235634892");
                startActivityForResult(i, ENDGAME_REQUEST_CODE);
                stopTimer();
            }
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    updateScoreText();
                    updateTimeText();
                    updateBlockText();
                }
            });
        }
    }
}
