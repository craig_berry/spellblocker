package edu.neu.madcourse.craigberry2.spellblocker;

import android.content.Context;
import android.telephony.TelephonyManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.regex.Pattern;


/**
 * Created by craig on 2/21/14.
 */
public class ScoreTracker {
    private static final String PLAYER_KEY = "neu.edu.madcourse.craigberry.spellblocker.player.";
    private static final String MASTER_LIST_KEY = "neu.edu.madcourse.craigberry.spellblocker.playerlist";
    private static final String ONE_PLAYER_EASY_TOP_SCORE_KEY = "neu.edu.madcourse.craigberry.spellblocker.singleplayer.easy.topscores";
    private static final String ONE_PLAYER_MEDIUM_TOP_SCORE_KEY = "neu.edu.madcourse.craigberry.spellblocker.singleplayer.medium.topscores";
    private static final String ONE_PLAYER_HARD_TOP_SCORE_KEY = "neu.edu.madcourse.craigberry.spellblocker.singleplayer.hard.topscores";
    private static final String COL_DELIMETER = "\\+";
    private static final String ROW_DELIMITER = "\\?";

    private static String getPlayerKey(String meid) {
        return PLAYER_KEY + meid;
    }

    private static String getRawPlayers() {
        return HealthServer.getValue(MASTER_LIST_KEY);
    }

    public static Player getMultiplayerProfile(String meid) {
        return Player.deserialize(HealthServer.getValue(getPlayerKey(meid)), COL_DELIMETER);
    }

    public static void storeMultiplayerProfile(Player player) {
        String key = PLAYER_KEY + player.getMeid();
        String entry = player.serialize(COL_DELIMETER);
        HealthServer.storeValue(key, entry);
        updatePlayerList(player.getMeid());
    }

    private static void updatePlayerList(String meid) {
        String players = getRawPlayers();
        players += COL_DELIMETER + meid;
        if (!HealthServer.storeValue(MASTER_LIST_KEY, players, true)) {
            try {
                throw new Exception("Error storing player id list");
            } catch (Exception e) {
                e.printStackTrace();
            }
        };
    }

    public static ArrayList<Player> getPlayerList() {
        String playerMeidsString = getRawPlayers();
        String[] playerMeidArr = playerMeidsString.split(COL_DELIMETER);
        ArrayList<Player> playerArrayList = new ArrayList<Player>();

        for (String meid : playerMeidArr) {
            String playerStr = HealthServer.getValue(getPlayerKey(meid));
            Player p = Player.deserialize(playerStr, COL_DELIMETER);
            playerArrayList.add(p);
        }

        return playerArrayList;
    }

    public static String updateHighScoreList(int diff, HighScore score) {
        String topScores = getSerializedScoreList(diff);
        ArrayList<HighScore> scoresArray;
        if (topScores.contains("Error") || topScores.equals("")) {
            scoresArray = new ArrayList<HighScore>();
        } else {
            scoresArray = buildHighScoreList(topScores);
        }
        scoresArray.add(score);
        sortScores(scoresArray);
        storeScoresList(diff, scoresArray);
        return getSerializedScoreList(diff);
    }

    private static void storeScoresList(int diff, ArrayList<HighScore> scoresArray) {
        String key = "";
        if (diff == Game.DIFFICULTY_EASY) {
            key = ONE_PLAYER_EASY_TOP_SCORE_KEY;
        } else if (diff == Game.DIFFICULTY_MEDIUM) {
            key = ONE_PLAYER_MEDIUM_TOP_SCORE_KEY;
        } else {
            key = ONE_PLAYER_HARD_TOP_SCORE_KEY;
        }
        HealthServer.storeValue(key, serializeScoreArray(scoresArray));
    }

    private static String serializeScoreArray(ArrayList<HighScore> scoresArray) {
        String data = "";
        for (HighScore h : scoresArray) {
            String score = h.serialize(COL_DELIMETER);
            data += score + ROW_DELIMITER;
        }
        return data;
    }

    public static String getSerializedScoreList(int diff) {
        String topScores = "";
        if (diff == Game.DIFFICULTY_EASY) {
            topScores = HealthServer.getValue(ONE_PLAYER_EASY_TOP_SCORE_KEY);
        } else if (diff == Game.DIFFICULTY_MEDIUM) {
            topScores = HealthServer.getValue(ONE_PLAYER_MEDIUM_TOP_SCORE_KEY);
        } else {
            topScores = HealthServer.getValue(ONE_PLAYER_HARD_TOP_SCORE_KEY);
        }
        return topScores;
    }

    public static ArrayList<HighScore> buildHighScoreList(String data) {
        ArrayList<HighScore> scoreArray = new ArrayList<HighScore>();
        String[] highScoresRows = data.split(ROW_DELIMITER);

        for (int i = 0; i < highScoresRows.length; i++) {
            scoreArray.add(HighScore.deserialize(highScoresRows[i], COL_DELIMETER));
        }

        return scoreArray;
    }

    private static ArrayList<HighScore> sortScores(ArrayList<HighScore> scoresArray) {
        Collections.sort(scoresArray);
        return updateIndex(scoresArray);
    }

    private static ArrayList<HighScore> updateIndex(ArrayList<HighScore> scoresArray) {
        ArrayList<HighScore> arr = new ArrayList<HighScore>();
        for (int i = 0; i < scoresArray.size(); i++) {
            HighScore score = scoresArray.get(i);
            score.setIndex(i+1);
            arr.add(i, score);
        }
        return arr;
    }

}
