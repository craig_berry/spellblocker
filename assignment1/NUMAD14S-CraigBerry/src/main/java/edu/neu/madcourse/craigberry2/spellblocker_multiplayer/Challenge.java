package edu.neu.madcourse.craigberry2.spellblocker_multiplayer;

/**
 * Created by craig on 3/22/14.
 */
public class Challenge {
    private Opponent challenger, contender;
    private String DELIMETER = ChallengeMode.CHALLENGE_DELIMETER;

    public Challenge(Opponent challenger, Opponent contender) {
        this.challenger = challenger;
        this.contender = contender;
    }

    public String toString() {
        return challenger.toString() + DELIMETER + contender.toString();
    }

    public boolean isCompleted() {
        return challenger.hasPlayed() && contender.hasPlayed();
    }

    /////////////////////////////////////////////////////////////
    // Getters and Setters
    /////////////////////////////////////////////////////////////

    public Opponent getChallenger() {
        return challenger;
    }

    public Opponent getContender() {
        return contender;
    }
}
