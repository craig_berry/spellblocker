package edu.neu.madcourse.craigberry2.spellblocker;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by craig on 2/22/14.
 */
public class HighScore implements Comparable<HighScore> {
    private static final int INDEX_COL = 0, MEID_COL = 1, NAME_COL = 2, SCORE_COL = 3, TIME_COL = 4, DATE_COL = 5;
    private static final String datetimeFormat = "yyyy.MM.dd HH:mm:ss z";
    private int index, score, time;
    private String name, meid;
    private Calendar datetime;

    public HighScore(String meid) {
        this.meid = meid;
        this.name = "Player";
        this.index = -1;
        this.score = 0;
        this.time = 0;
        this.datetime = Calendar.getInstance();
    }

    // Deserialize, takes an encoded string and turns it into a HighScore java object
    public static HighScore deserialize(String scoreStr, String delimeter) {
        String[] entryArr = scoreStr.split(delimeter);
        HighScore h = new HighScore(entryArr[MEID_COL]);
        h.setName(entryArr[NAME_COL]);
        h.setIndex(Integer.parseInt(entryArr[INDEX_COL]));
        h.setScore(Integer.parseInt(entryArr[SCORE_COL]));
        h.setTime(Integer.parseInt(entryArr[TIME_COL]));

        Calendar cal = h.getDatetime();
        SimpleDateFormat sdf = new SimpleDateFormat(datetimeFormat);
        try {
            cal.setTime(sdf.parse(entryArr[DATE_COL]));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return h;
    }

    public String serialize(String delimeter) {
        String[] entryArr = new String[6];  // Number of columns in high score entry
        entryArr[INDEX_COL] = getIndex() + "";
        entryArr[MEID_COL] = getMeid();
        entryArr[NAME_COL] = getName();
        entryArr[TIME_COL] = getTime() + "";
        entryArr[SCORE_COL] = getScore() + "";

        SimpleDateFormat dateFormat = new SimpleDateFormat(datetimeFormat);
        try {
            Date date = getDatetime().getTime();
            entryArr[DATE_COL] = dateFormat.format(date);
        } catch (Exception e){

        }

        return buildString(entryArr, delimeter);
    }

    public static String buildString(String[] arr, String delimeter) {
        String result = "";

        for (String s : arr) {
            result += s + delimeter;
        }
        return result;
    }

    // Getters and Setters
    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getDatetime() {
        return datetime;
    }

    public String getMeid() {
        return meid;
    }

//    public void setDatetime(Calendar datetime) {
//        this.datetime = datetime;
//    }

    @Override
    public int compareTo(HighScore that) {
        if (this.getScore() > that.getScore()) {
            return -1;
        } else if (this.getScore() < that.getScore()) {
            return 1;
        } else {
            return 0;
        }
    }
}