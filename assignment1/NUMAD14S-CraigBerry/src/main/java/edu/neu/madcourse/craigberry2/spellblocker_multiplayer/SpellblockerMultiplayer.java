package edu.neu.madcourse.craigberry2.spellblocker_multiplayer;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import edu.neu.madcourse.craigberry2.R;

public class SpellblockerMultiplayer extends ActionBarActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spellblocker_multiplayer);

        setupClickListeners();
    }

    private void setupClickListeners() {
        // Set up click listeners for all the buttons
        View findGameButton = findViewById(R.id.multi_find_game_button);
        findGameButton.setOnClickListener(this);
        View profileButton = findViewById(R.id.multi_profile_button);
        profileButton.setOnClickListener(this);
        View ackButton = findViewById(R.id.multi_ack_button);
        ackButton.setOnClickListener(this);
        View exitButton = findViewById(R.id.multi_exit_button);
        exitButton.setOnClickListener(this);
    }


    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.multi_find_game_button:
                openFindGameDialog();
                break;
            case R.id.multi_profile_button:
                Intent instructions = new Intent(this, Profile.class);
                startActivity(instructions);
                break;
            case R.id.multi_ack_button:
                Intent ack = new Intent(this, Acknowledgements.class);
                startActivity(ack);
                break;
            case R.id.multi_exit_button:
                finish();
                break;
        }
    }

    private void openFindGameDialog() {
        String[] arr = new String[2];
        arr[0] = "Battle";
        arr[1] = "Challenge";

        new AlertDialog.Builder(this)
            .setTitle("Select Mode")
            .setItems(arr,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (i == 0) {
                                startBattle();
                            }
                            if (i == 1) {
                                startChallenge();
                            }
                        }
                    })
            .show();
    }

    private void startChallenge() {
        Intent challengeIntent = new Intent(getBaseContext(), ChallengeMode.class);
        startActivity(challengeIntent);
    }

    private void startBattle() {
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.two_player_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
