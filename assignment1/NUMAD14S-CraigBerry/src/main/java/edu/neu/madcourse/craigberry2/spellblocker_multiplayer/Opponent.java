package edu.neu.madcourse.craigberry2.spellblocker_multiplayer;

import java.lang.reflect.Array;

/**
 * Created by craig on 3/22/14.
 */
public class Opponent {
    private String meid, DELIMETER = ChallengeMode.OPPONENT_DELIMETER;
    private int score, time, words;
    private boolean hasPlayed;

    public Opponent(String meid) {
        this.meid = meid;
        this.score = -1;
        this.time = -1;
        this.words = -1;
        this.hasPlayed = false;
    }

    public String toString() {
        return this.meid + DELIMETER +
               this.hasPlayed + DELIMETER +
               this.score + DELIMETER +
               this.time + DELIMETER +
               this.words + "";
    }

    public Opponent deserialize(String opponentStr) {
        String[] arr = opponentStr.split(DELIMETER);
        Opponent op = new Opponent(arr[0]);
        op.setHasPlayed(new Boolean(arr[1]));
        op.setScore(Integer.parseInt(arr[2]));
        op.setTime(Integer.parseInt(arr[3]));
        op.setWords(Integer.parseInt(arr[4]));

        return op;
    }

    /////////////////////////////////////////////////////////////
    // Server Functions
    /////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////
    // Getters and Setters
    /////////////////////////////////////////////////////////////

    public String getMeid() {
        return meid;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getWords() {
        return words;
    }

    public void setWords(int words) {
        this.words = words;
    }

    public boolean hasPlayed() {
        return hasPlayed;
    }

    public void setHasPlayed(boolean hasPlayed) {
        this.hasPlayed = hasPlayed;
    }
}
