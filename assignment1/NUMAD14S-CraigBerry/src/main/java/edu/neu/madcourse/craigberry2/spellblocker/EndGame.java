/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.craigberry2.spellblocker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;


import android.util.Log;

import edu.neu.madcourse.craigberry2.AboutActivity;
import edu.neu.madcourse.craigberry2.R;

public class EndGame extends Activity {
    SharedPreferences prefs;
    boolean isWin = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spellblocker_endgame);
        Intent intent = getIntent();
        prefs = this.getSharedPreferences(Game.PREF_KEY, Context.MODE_PRIVATE);
        prefs.edit().putBoolean(Game.PREF_CONT, false).commit();

        isWin = intent.getBooleanExtra(Game.WON_EXTRA, false);
        int score = intent.getIntExtra(Game.SCORE_EXTRA, -1),
            left = intent.getIntExtra(Game.BLOCK_LEFT_EXTRA, -1),
            total = intent.getIntExtra(Game.BLOCK_TOTAL_EXTRA, -1),
            time = intent.getIntExtra(Game.TIME_EXTRA, 0),
            words = intent.getIntExtra(Game.WORD_NUM_EXTRA, 0),
            difficulty = intent.getIntExtra(Game.DIFF_EXTRA, Game.DIFFICULTY_EASY);
        String meid = intent.getStringExtra(Game.MEID_EXTRA);

        String eol = System.getProperty("line.separator");
        TextView t = (TextView) findViewById(R.id.sb_endgame_text);
        if (isWin) {
            setTitleColor(Color.GREEN);
            setTitle("Game Over: You Win");

            // Update High Score Lists, test popping up message box from async task
            new UpdateScoreTask().execute(intent);
        }
        else {
            setTitleColor(Color.RED);
            setTitle("Game Over: You Lose");
            t.setText("Time Up!" + eol + "Score: " + score + eol + "Blocks Cleared: " + (total - left) + eol + "Blocks Left: " + left + eol + eol + "Better luck next time!");
        }
    }

    @Override
    public void finish() {
        Intent data = new Intent();
        data.putExtra(Game.WON_EXTRA, isWin);
        setResult(Game.EXIT_RESULT_CODE, data);

        super.finish();
    }

    public class UpdateScoreTask extends AsyncTask<Intent, Integer, String> {

        @Override
        protected String doInBackground(Intent... params) {
            Intent intent = params[0];
            int score = intent.getIntExtra(Game.SCORE_EXTRA, -1),
                    time = intent.getIntExtra(Game.TIME_EXTRA, 0),
                    words = intent.getIntExtra(Game.WORD_NUM_EXTRA, 0),
                    difficulty = intent.getIntExtra(Game.DIFF_EXTRA, Game.DIFFICULTY_EASY);
            String meid = intent.getStringExtra(Game.MEID_EXTRA);

            // Update high scores
            HighScore h = new HighScore(meid);
            h.setScore(score);
            h.setTime(time);
            h.getDatetime();
            Log.d("HIGH_SCORE",  ScoreTracker.updateHighScoreList(difficulty, h));
            String eol = System.getProperty("line.separator");

            return "Time: " + time + eol + "Score: " + score + eol + "Words Entered: " + words + eol + "Rank: " + h.getIndex() + eol + eol + "Great Job!";
        }

        @Override
        protected void onPostExecute(String result) {
            TextView t = (TextView) findViewById(R.id.sb_endgame_text);
            setTitleColor(Color.GREEN);
            setTitle("Game Over: You Win");
            t.setText(result);
        }

    }
}
