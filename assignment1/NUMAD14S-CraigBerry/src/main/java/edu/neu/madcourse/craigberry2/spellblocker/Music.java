/***
 * Excerpted from "Hello, Android",
 * published by The Pragmatic Bookshelf.
 * Copyrights apply to this code. It may not be used to create training material, 
 * courses, books, articles, and the like. Contact us if you are in doubt.
 * We make no guarantees that this code is fit for any purpose. 
 * Visit http://www.pragmaticprogrammer.com/titles/eband3 for more book information.
 ***/
package edu.neu.madcourse.craigberry2.spellblocker;

import android.content.Context;
import android.media.MediaPlayer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import static android.media.MediaPlayer.OnCompletionListener;

public class Music {
    private static MediaPlayer mp = null;
    private static MediaPlayer click = null;
    private static MediaPlayer correct = null;
    private static MediaPlayer incorrect = null;
    private static MediaPlayer fail = null;
    private static MediaPlayer pause = null;
    private static MediaPlayer unpause = null;
    private static MediaPlayer bigClear = null;

    /**
     * Stop old song and start new one
     */
    public static void play(Context context, int resource) {
        stop();

        // Start music only if not disabled in preferences
        if (Prefs.getMusic(context)) {
            mp = MediaPlayer.create(context, resource);
            mp.setLooping(true);
            mp.start();
        }
    }

    /**
     * Stop the music
     */
    public static void stop() {
        if (mp != null) {
            mp.stop();
            mp.release();
            mp = null;
        }
    }

    // Play clicking sound on type
    public static void playClick(Context context, int resource) {
       

        // Start music only if not disabled in preferences
        if (Prefs.getMusic(context)) {
            click = MediaPlayer.create(context, resource);
            click.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer click) {
                    if (click != null) {
                        click.release();
                    }
                }
            });
            click.setLooping(false);
            click.start();
        }
    }

    // Play sound to indicate failed block move
    public static void playFailed(Context context, int resource) {
       

        // Start music only if not disabled in preferences
        if (Prefs.getMusic(context)) {
            fail = MediaPlayer.create(context, resource);
            fail.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer fail) {
                    if (fail != null) {
                        fail.release();
                    }
                }
            });
            fail.setLooping(false);
            fail.start();
        }
    }

    // Play sound to indcate correct word entered
    public static void playCorrect(Context context, Integer[] sounds) {
       

        // Start music only if not disabled in preferences
        if (Prefs.getMusic(context)) {
            Random r = new Random();
            int resource = sounds[r.nextInt(sounds.length)];
            correct = MediaPlayer.create(context, resource);
            correct.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer correct) {
                    if (correct != null) {
                        correct.release();
                    }
                }
            });
            correct.setLooping(false);
            correct.start();
        }
    }

    public static void playPause(Context context, int resource) {
       

        // Start music only if not disabled in preferences
        if (Prefs.getMusic(context)) {
            pause = MediaPlayer.create(context, resource);
            pause.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer pause) {
                    if (pause != null) {
                        pause.release();
                    }
                }
            });
            pause.setLooping(false);
            pause.start();
        }
    }

    // Formerly used as the "unpause" sound, currently a placeholder for the "starting" sound
    public static void playUnpause(Context context, int resource) {
       

        // Start music only if not disabled in preferences
        if (Prefs.getMusic(context)) {
            unpause = MediaPlayer.create(context, resource);
            unpause.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer unpause) {
                    if (unpause != null) {
                        unpause.release();
                    }
                }
            });
            unpause.setLooping(false);
            unpause.start();
        }
    }

    // Indicate word was not accepted
    public static void playIncorrect(Context context, int resource) {
       

        // Start music only if not disabled in preferences
        if (Prefs.getMusic(context)) {
            incorrect = MediaPlayer.create(context, resource);
            incorrect.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer incorrect) {
                    if (incorrect != null) {
                        incorrect.release();
                    }
                }
            });
            incorrect.setLooping(false);
            incorrect.start();
        }
    }

    // Play sound when lines are cleared.
    public static void playBigClear(Context context, int resource) {


        // Start music only if not disabled in preferences
        if (Prefs.getMusic(context)) {
            bigClear = MediaPlayer.create(context, resource);
            bigClear.setOnCompletionListener(new OnCompletionListener() {

                @Override
                public void onCompletion(MediaPlayer bigClear) {
                    if (bigClear != null) {
                        bigClear.release();
                    }
                }
            });
            bigClear.setLooping(false);
            bigClear.start();
        }
    }

//

//
//    public static void playSound(Context context, int sound) {
//        if (Prefs.getMusic(context)) {
//            MediaPlayer player = MediaPlayer.create(context, sound);
//            Music.setupCompletionListener(player);
//            player.setLooping(false);
//            player.start();
//
//        }
//    }
//
//    // Setup sounds to end
//    private static void setupCompletionListener(MediaPlayer player) {
//        player.setOnCompletionListener(new OnCompletionListener() {
//
//            @Override
//            public void onCompletion(MediaPlayer mp) {
//
//                if (mp.isPlaying()) {
//                    mp.release();
//                }
//            }
//        });
//    }
}
