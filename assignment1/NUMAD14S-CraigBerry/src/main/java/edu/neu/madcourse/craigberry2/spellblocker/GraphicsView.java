
package edu.neu.madcourse.craigberry2.spellblocker;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;

import edu.neu.madcourse.craigberry2.R;


public class GraphicsView extends View {

    // Bundle tags
    private static final String TAG = "Sudoku";
    private static final String SELX = "neu.edu.madcourse.craigberry.spellblocker.selX";
    private static final String SELY = "neu.edu.madcourse.craigberry.spellblocker.selY";
    private static final String SCORE = "neu.edu.madcourse.craigberry.spellblocker.score";
    private static final String TIME = "neu.edu.madcourse.craigberry.spellblocker.time";
    private static final String PUZZLE = "neu.edu.madcourse.craigberry.spellblocker.puzzle";
    private static final String BLKS_LEFT = "neu.edu.madcourse.craigberry.spellblocker.blocksleft";
    private static final String BLKS_TOTAL = "neu.edu.madcourse.craigberry.spellblocker.blockstotal";
    private static final String COLOR_MAP = "neu.edu.madcourse.craigberry.spellblocker.colormap";
    private static final String HILITE_BLKS = "neu.edu.madcourse.craigberry.spellblocker.hiliteblocks";
    private static final String LETTERS = "neu.edu.madcourse.craigberry.spellblocker.letters";
    private static final String WORD_COUNT = "neu.edu.madcourse.craigberry.spellblocker.wordcount";
    private static final String CUR_WORD = "neu.edu.madcourse.craigberry.spellblocker.currentword";
    private static final String VIEW_STATE = "neu.edu.madcourse.craigberry.spellblocker.viewState";

    // Initialize
    private final Game game;
    private final Rect selRect = new Rect();
    protected int PUZZLE_HEIGHT; // Grid Height in # squares
    protected int PUZZLE_WIDTH;  // Grid width in # squares
    private float gridWidth;
    private float gridHeight;
    private float sqWidth;    // width of one tile
    private float sqHeight;   // height of one tile
    private float xOff;      // Offset in x plane
    private float yOff;      // Offset in y plane
    private Point selection;
    private boolean isFirstTouch = true;
    private boolean isDoubleTap = false;

    public GraphicsView(Context context) {
        super(context);
        this.game = (Game) context;
        selection = new Point(-1, -1);
        init();
    }

    public GraphicsView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.game = (Game) context;
        selection = new Point(-1, -1);
        init();
    }

    public GraphicsView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.game = (Game) context;
        selection = new Point(-1, -1);
        init();
    }

    // Getters & Setters
    private boolean isFirstTouch() {
        return isFirstTouch;
    }

    private void endFirstTouch() {
        isFirstTouch = false;
    }

    private boolean isDoubleTap() {
        return isDoubleTap;
    }

    public void setDoubleTap(boolean val) {
        isDoubleTap = val;
    }

    private void turnDoubleTapOn() {
        setDoubleTap(true);
    }

    private void turnDoubleTapOff() {
        wasDoubleTap = true;
        setDoubleTap(false);
    }

    public Point getSelection() {
        return selection;
    }

    public void init() {
        setFocusable(true);
        setFocusableInTouchMode(true);
        PUZZLE_HEIGHT = game.PUZZLE_HEIGHT;
        PUZZLE_WIDTH = game.PUZZLE_WIDTH;
        isFirstTouch = true;
        isDoubleTap = false;
    }

    private void calcAndSetDimensions(int width, int height) {
        xOff = 0; // Spaces to cleared from EITHER SIDE of grid (i.e. 1/8 = 1/4 total)
        yOff = 0; // Space on top (affects scoring placement, move to xml layout?)
        gridWidth = width - (2 * xOff);
        gridHeight = height - yOff;
        sqWidth = gridWidth / (float) PUZZLE_WIDTH;
        sqHeight = gridHeight / (float) PUZZLE_HEIGHT;
    }

    @Override
    protected Parcelable onSaveInstanceState() {
        Parcelable p = super.onSaveInstanceState();
        Log.d(TAG, "onSaveInstanceState");
        SharedPreferences sp = getContext().getSharedPreferences("fda", Context.MODE_PRIVATE);
        sp.edit();

        Bundle bundle = new Bundle();
        bundle.putString(CUR_WORD, game.getCurrentWord());
        bundle.putInt(SELX, selection.x);
        bundle.putInt(SELY, selection.y);
        bundle.putInt(SCORE, game.getScore());
        bundle.putInt(TIME, game.getTime());
        bundle.putInt(WORD_COUNT, game.getTotalEnteredWords());
        bundle.putInt(BLKS_LEFT, game.getRemainingBlocks());
        bundle.putInt(BLKS_TOTAL, game.getTotalBlocks());
        bundle.putSerializable(COLOR_MAP, game.getSquareColors());
        bundle.putSerializable(HILITE_BLKS, game.getHighlightedList());
        bundle.putSerializable(LETTERS, game.getSelectedList());
        bundle.putSerializable(PUZZLE, game.getPuzzle());
        bundle.putParcelable(VIEW_STATE, p);

        return bundle;
    }

    @Override
    protected void onRestoreInstanceState(Parcelable state) {
        Log.d(TAG, "onRestoreInstanceState");
        Bundle bundle = (Bundle) state;

        select(bundle.getInt(SELX), bundle.getInt(SELY));
        game.setCurrentWord(bundle.getString(CUR_WORD));
        game.setScore(bundle.getInt(SCORE));
        game.setTime(bundle.getInt(TIME));
        game.setTotalEnteredWords(bundle.getInt(WORD_COUNT));
        game.setRemainingBlocks(bundle.getInt(BLKS_LEFT));
        game.setTotalBlocks(bundle.getInt(BLKS_TOTAL));
        game.setSquareColors((HashMap<Point, Integer>) bundle.getSerializable(COLOR_MAP));
        game.setHighlightedSquares((ArrayList<Point>) bundle.getSerializable(HILITE_BLKS));
        game.setSelectedSquares((ArrayList<Point>) bundle.getSerializable(LETTERS));
        game.setPuzzle((String[]) bundle.getSerializable(PUZZLE));

        ArrayList<Point> test = game.getSelectedList();
        super.onRestoreInstanceState(bundle.getParcelable(VIEW_STATE));
        invalidate();
    }

    public void shakeBoardAnimation() {
        startAnimation(AnimationUtils.loadAnimation(game, R.anim.shake));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {

        calcAndSetDimensions(w, h);
        getRect(selection.x, selection.y, selRect);
        Log.d(TAG, "onSizeChanged: width " + w + ", height "
                + h);
        super.onSizeChanged(w, h, oldw, oldh);
    }

    protected void drawHighlightedSquares(Canvas canvas) {
        // COLOR
        Paint selPaint = new Paint();
        selPaint.setStyle(Style.FILL);

        // Draw highlighted squares
        for (int i = 0; i < PUZZLE_WIDTH; i++) {
            for (int j = 0; j < PUZZLE_HEIGHT; j++) {
                Point point = new Point(i, j);
                if (game.isHighlighted(point)) {
                    selPaint.setColor(game.getSquareColor(point));
                    float leftSide = xOff + i * sqWidth;
                    float topSide = yOff + j * sqHeight;
                    canvas.drawRect(leftSide, topSide, leftSide + sqWidth, topSide + sqHeight, selPaint);
                }
            }
        }
    }

    protected void drawGrid(Canvas canvas) {
        // COLOR
        // Define colors for the grid lines
        Paint dark = new Paint(),
                light = new Paint(),
                hilite = new Paint();

        // Set Colors
        dark.setColor(getResources().getColor(R.color.puzzle_dark));
        hilite.setColor(getResources().getColor(R.color.puzzle_hilite));
        light.setColor(getResources().getColor(R.color.puzzle_light));

        // Draw the X gridlines
        for (int x = 0; x < PUZZLE_WIDTH; x++) {
            canvas.drawLine(xOff + x * sqWidth, yOff, xOff + x * sqWidth, yOff + gridHeight, light);
            canvas.drawLine(xOff + x * sqWidth + 1, yOff, xOff + x * sqWidth + 1, yOff + gridHeight, hilite);
            canvas.drawLine(xOff + x * sqWidth, yOff, xOff + x * sqWidth, yOff + gridHeight, dark);
            canvas.drawLine(xOff + x * sqWidth + 1, yOff, xOff + x * sqWidth + 1, yOff + gridHeight, hilite);
        }

        // Draw the  Y gridlines
        for (int y = 0; y < PUZZLE_HEIGHT; y++) {
            canvas.drawLine(xOff, yOff + y * sqHeight, xOff + gridWidth, yOff + y * sqHeight, light);
            canvas.drawLine(xOff, yOff + y * sqHeight + 1, xOff + gridWidth, yOff + y * sqHeight + 1, hilite);
            canvas.drawLine(xOff, yOff + y * sqHeight, xOff + gridWidth, yOff + y * sqHeight, dark);
            canvas.drawLine(xOff, yOff + y * sqHeight + 1, xOff + gridWidth, yOff + y * sqHeight + 1, hilite);
        }

    }


    @Override
    protected void onDraw(Canvas canvas) {

        // Possibly only needs to call once OnSizeChanged handles rest?
        calcAndSetDimensions(getWidth(), getHeight());

        if (game.isRunning()) {
            // Background
            drawBackground(canvas);

            // Highlight squares
            drawHighlightedSquares(canvas);

            // Draw main grid and outline squares
            drawGrid(canvas);

            // Draw square contents && draws score
            drawSquareText(canvas);

            // Most recently clicked square
            drawSelectedSquare(canvas);

            // Draws a hint if necessary
            drawHint(canvas);
        }
    }

    private void drawBackground(Canvas canvas) {
        // Draw Grid background
        Paint background = new Paint();
        background.setColor(getResources().getColor(R.color.puzzle_background));
        canvas.drawRect(xOff, yOff, xOff + gridWidth, yOff + gridHeight, background);
    }

    // Draws selected square, ignores highlighted squares unless double tapped
    private void drawSelectedSquare(Canvas canvas) {
        // Draw the selection...
        Log.d(TAG, "selRect=" + selRect);
        Paint selected = new Paint();

        if (isDoubleTap() && game.isHighlighted(selection)) {

            Shader shader = new LinearGradient(xOff + selection.x*sqWidth, yOff + selection.y * sqHeight,
                    xOff + (selection.x + 1) * sqWidth, yOff + (1 + selection.y) * sqHeight, game.getSquareColor(selection), Color.BLACK, Shader.TileMode.CLAMP);
            Paint paint = new Paint();
            paint.setShader(shader);
            canvas.drawRect(selRect, paint);


//            int squareColor = game.getSquareColor(selection);
//            int inverseColor = Color.argb(255, 255 - Color.red(squareColor),
//                    255 - Color.green(squareColor),
//                    255 - Color.blue(squareColor));
//            selected.setColor(inverseColor);
//            selected.setStrokeWidth(selected.getStrokeWidth() * 3);
//            selected.setStyle(Style.FILL);
//            canvas.drawRect(selRect, selected);
        } else if (!game.isHighlighted(selection) && game.inBounds(selection)) {
            selected.setColor(getResources().getColor(R.color.puzzle_selected));
            selected.setStyle(Style.FILL);
            canvas.drawRect(selRect, selected);
        }
    }

    private void drawSquareText(Canvas canvas) {
        // Define color and style for numbers
        Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
        foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
        foreground.setStyle(Style.FILL);
        foreground.setTextSize(sqHeight * 0.75f);
        foreground.setTextScaleX(sqWidth / sqHeight);
        foreground.setTextAlign(Paint.Align.CENTER);

        // Draw the number in the center of the tile
        FontMetrics fm = foreground.getFontMetrics();
        // Centering in X: use alignment (and X at midpoint)
        float xCenter = sqWidth / 2;
        // Centering in Y: measure ascent/descent first
        float yCenter = sqHeight / 2 - (fm.ascent + fm.descent) / 2;
        for (int x = 0; x < PUZZLE_WIDTH; x++) {
            for (int y = 0; y < PUZZLE_HEIGHT; y++) {
                Point currentPoint = new Point(x, y);
                if (!game.isHighlighted(currentPoint)) {
                    if (game.isSelected(currentPoint)) {
                        foreground.setColor(Color.RED);
                    } else {
                        foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
                    }
                    canvas.drawText(this.game.getTileString(new Point(x, y)), x * sqWidth + xCenter + xOff, y * sqHeight + yCenter + yOff, foreground);
                }
            }
        }
    }

    protected void drawHint(Canvas canvas) {
        if (game.showHint) {
            game.showHint = false;
            Point hint = game.hint;

            // Vars for drawing hinted square
            float x = xOff + (hint.x * sqWidth);
            float y = yOff + (hint.y * sqHeight);
            float x1 = (sqWidth)/5;
            float x2 = (2*sqWidth)/5;
            float x3 = (3*sqWidth)/5;
            float x4 = (4*sqWidth)/5;
            float x5 = sqWidth;
            float y1 = (sqHeight)/5;
            float y2 = (2*sqHeight)/5;
            float y3 = (3*sqHeight)/5;
            float y4 = (4*sqHeight)/5;
            float y5 = sqHeight;

            Paint p = new Paint();
            p.setColor(Color.BLUE);
            canvas.drawRect(x, y, x + x1, y + y1, p);
            canvas.drawRect(x + x4, y, x + x5, y + y1, p);

            p.setColor(Color.GREEN);
            canvas.drawRect(x + x1, y + y1 , x + x2, y + y2, p);
            canvas.drawRect(x + x3, y + y1 , x + y4, y + y2, p);

            p.setColor(Color.RED);
            canvas.drawRect(x + x2, y + y2, x + x3, y + y3, p);

            p.setColor(Color.YELLOW);
            canvas.drawRect(x + x3, y + y3, x + x4, y + y4, p);
            canvas.drawRect(x + x1, y + y3, x + x2, y + y4, p);

            p.setColor(Color.CYAN);
            canvas.drawRect(x + x4, y + y4, x + x5, y + y5, p);
            canvas.drawRect(x, y + y4, x + x1, y + y5, p);
        }
    }

    boolean wasDoubleTap = false;

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() != MotionEvent.ACTION_DOWN)
            return super.onTouchEvent(event);

        // Set selection
        select((int) ((event.getX() - xOff) / sqWidth),
                (int) ((event.getY() - yOff) / sqHeight));

        if (clickInBounds(event.getX(), event.getY())) {
            // Process doubletaps/initial settings
            processTapEvents();
            // Process actual click
            processBoardClick();

        }
        // Past tense hack used for preventing selection of new letters when trying to move colored blocks
        wasDoubleTap = false;
        return true;
    }

    private void processBoardClick() {
        game.previousSelection = new Point(selection.x, selection.y);
        if (!(game.isHighlighted(selection) || game.isSelected(selection) || isDoubleTap() || wasDoubleTap)) {
            Music.playClick(this.game, R.raw.sb_click);
            game.addSelectedSquare(new Point(selection.x, selection.y), Color.BLACK);
            TextView userInputTextView = (TextView) this.game.findViewById(R.id.wgInputTextView);
            userInputTextView.setText(userInputTextView.getText() + this.game.getTileString(selection));
            game.setCurrentWord(userInputTextView.getText().toString());
        }
    }

    private void processTapEvents() {

        // Clears 'Start!' text from input bar
        if (isFirstTouch()) {
            TextView userInputTextView = (TextView) this.game.findViewById(R.id.wgInputTextView);
            userInputTextView.setText("");
            endFirstTouch();
        }

        // If first pass since isDoubleTap turned on
        if (isDoubleTap()) {
            // Pass to game to process, switch off
            game.updateDoubleTap(selection);
            turnDoubleTapOff();
        }

        // If selections is same as last and its highlighted
        if (game.previousSelection.equals(selection) && game.isHighlighted(selection)) {
            // Prime doubleTap for next on touch
            turnDoubleTapOn();
        }
    }

    private boolean clickInBounds(float mouseX, float mouseY) {
        return mouseX > xOff && mouseX < (gridWidth + xOff) && mouseY > yOff;
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        Log.d(TAG, "onKeyDown: keycode=" + keyCode + ", event="
                + event);
        int selX = selection.x;
        int selY = selection.y;
        switch (keyCode) {
            case KeyEvent.KEYCODE_DPAD_UP:
                select(selX, selY - 1);
                break;
            case KeyEvent.KEYCODE_DPAD_DOWN:
                select(selX, selY + 1);
                break;
            case KeyEvent.KEYCODE_DPAD_LEFT:
                select(selX - 1, selY);
                break;
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                select(selX + 1, selY);
                break;
            case KeyEvent.KEYCODE_0:
            case KeyEvent.KEYCODE_SPACE:
                setSelectedTile(0);
                break;
            case KeyEvent.KEYCODE_1:
                setSelectedTile(1);
                break;
            case KeyEvent.KEYCODE_2:
                setSelectedTile(2);
                break;
            case KeyEvent.KEYCODE_3:
                setSelectedTile(3);
                break;
            case KeyEvent.KEYCODE_4:
                setSelectedTile(4);
                break;
            case KeyEvent.KEYCODE_5:
                setSelectedTile(5);
                break;
            case KeyEvent.KEYCODE_6:
                setSelectedTile(6);
                break;
            case KeyEvent.KEYCODE_7:
                setSelectedTile(7);
                break;
            case KeyEvent.KEYCODE_8:
                setSelectedTile(8);
                break;
            case KeyEvent.KEYCODE_9:
                setSelectedTile(9);
                break;
            case KeyEvent.KEYCODE_ENTER:
            case KeyEvent.KEYCODE_DPAD_CENTER:
                //  game.showKeypadOrError(selX, selY);
                break;
            default:
                return super.onKeyDown(keyCode, event);
        }
        return true;
    }

    public void setSelectedTile(int tile) {
//      if (game.setTileIfValid(selX, selY, tile)) {
//         invalidate();// may change hints
//      } else {
//         // Number is not valid for this tile
//         Log.d(TAG, "setSelectedTile: invalid: " + tile);
//         startAnimation(AnimationUtils.loadAnimation(game, R.anim.shake));
//      }
    }

    // Select new square, invalidate new/old
    private void select(int x, int y) {
        invalidate(selRect);
        if (game.inBounds(new Point(x, y))) {
            selection.set(x, y);
            getRect(x, y, selRect);
        } else {
            selection.set(-1, -1);
            turnDoubleTapOff();
            selRect.set(0, 0, 0, 0);
        }
        invalidate(selRect);
    }

    private void getRect(int x, int y, Rect rect) {
        rect.set((int) (xOff + x * sqWidth), (int) (yOff + y * sqHeight), (int) (xOff + x
                * sqWidth + sqWidth), (int) (yOff + y * sqHeight + sqHeight));
    }


    // ...
}

// OLD HINT
      /*
      if (Prefs.getHints(getContext())) {
         // Draw the hints...

         // Pick a hint color based on #moves left
         Paint hint = new Paint();
         int c[] = { getResources().getColor(R.color.puzzle_hint_0),
               getResources().getColor(R.color.puzzle_hint_1),
               getResources().getColor(R.color.puzzle_hint_2), };
         Rect r = new Rect();
         for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
               int movesleft = 9 - game.getUsedTiles(i, j).length;
               if (movesleft < c.length) {
                  getRect(i, j, r);
                  hint.setColor(c[movesleft]);
                  canvas.drawRect(r, hint);
               }
            }
         }

      }*/