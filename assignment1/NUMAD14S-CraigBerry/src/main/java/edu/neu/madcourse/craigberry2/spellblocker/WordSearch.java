package edu.neu.madcourse.craigberry2.spellblocker;

import android.content.res.Resources;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import edu.neu.madcourse.craigberry2.R;

public class WordSearch {
    protected int WORD_MAP_KEY_LENGTH; // Number of letters wordMap uses for keys
    protected int WORD_FILE_GRANULARITY; // Number of letters the word files are stored as
    protected HashMap<String, ArrayList<String>> wordMap; // Store array list of words with like key of specified granularity
    protected ArrayList<String> foundWordsArr; // Keep track of words that have been successfully found
    protected boolean dictionaryLoaded; // Has the dictionary been loaded
    protected String previousFileLookup; // Record the most recent file lookup performed
    String currentKey; // Current key of word file parsing buffer
    ArrayList<String> sameKeyWordsArr = new ArrayList<String>(); // Storage for words going in the same hasmap entry
    protected Resources resources; // Dictionary file resources given by constructor

    public WordSearch(Resources resources) {
        this.resources = resources;
        previousFileLookup = "/";
        WORD_MAP_KEY_LENGTH = 3;
        WORD_FILE_GRANULARITY = 2;
        wordMap = new HashMap<String, ArrayList<String>>();
        foundWordsArr = new ArrayList<String>();
        dictionaryLoaded = false;
    }

    public WordSearch(Resources resources, int wordKeyLength, int letterFileGranularity) {
        this.resources = resources;
        previousFileLookup = "/";
        WORD_MAP_KEY_LENGTH = wordKeyLength;
        WORD_FILE_GRANULARITY = letterFileGranularity;
        wordMap = new HashMap<String, ArrayList<String>>();
        foundWordsArr = new ArrayList<String>();
        dictionaryLoaded = false;
    }

    public ArrayList<String> getFoundWords() {
        return (ArrayList<String>) this.foundWordsArr.clone();
    }

    public void clearFoundWords() {
        foundWordsArr = null;
        foundWordsArr = new ArrayList<String>();
    }

    public boolean containsKey(String letters) {
        return letters.length() > WORD_MAP_KEY_LENGTH && containsWord(letters, true);
    }

    public boolean isWord(String letters) {
        return containsWord(letters, false);
    }

    // 'isStringBadCheck' is a switch that differentiates contains vs containsKey, and basically
    // says that containsKey will return true if the given string is a substring of a valid word,
    // whereas contains returns if it actually is a valid word.
    public boolean containsWord(String letters, boolean isStringBadCheck) {
        String userInput = letters.toLowerCase();
        if (foundWordsArr.contains(letters)) {
            return true;
        }
        // If input reaches minimum search length
        if (userInput.length() >= WORD_FILE_GRANULARITY) {
            String fileLookupKey = userInput.substring(0, WORD_FILE_GRANULARITY);
            // Load the dictionary if it isn't already loaded, or if we must change word files
            if (!dictionaryLoaded || !previousFileLookup.equals(fileLookupKey)) {
                cleanDictionary();
                loadDictionary(fileLookupKey);
            }
            // Search for words minimum length 3
            if (userInput.length() > 2) {
                if (searchForWord(userInput, isStringBadCheck)) {
                    return true;
                }
                previousFileLookup = fileLookupKey;
            }
        }
        return false;
    }

    private void cleanDictionary() {
        wordMap = null;
        wordMap = new HashMap<String, ArrayList<String>>();
        dictionaryLoaded = false;
    }

    private boolean searchForWord(String userInput, boolean isStringBadCheck) {
        String wordKey = userInput.substring(0, WORD_MAP_KEY_LENGTH);
        if (wordMap.containsKey(wordKey)) {
            ArrayList<String> arr = wordMap.get(wordKey);
            if (arr.contains(userInput)) {
                if (!foundWordsArr.contains(userInput)) {
                    foundWordsArr.add(userInput);
                }
                return true;
            }
            return isStringBadCheck && isRootOfWord(userInput, arr);
        }
        return false;
    }

    private boolean isRootOfWord(String userInput, ArrayList<String> arr) {
        for (String word : arr) {
            if (word.contains(userInput)) {
                return true;
            }
        }
        return false;
    }

    private void loadDictionary(String letter) {
        try {
            wordMap = new HashMap<String, ArrayList<String>>();
            parseWordList(getWordReader(letter), letter);
        } catch (IOException e) {

        }
        dictionaryLoaded = true;
    }

    private BufferedReader getWordReader(String inputString) {
        InputStream inputStream = resources.openRawResource(getRawWordList(inputString));
        return new BufferedReader(new InputStreamReader(inputStream));
    }

    private int getRawWordList(String letter) {

        try {
            if (letter.equals("do") || letter.equals("if")) {
                letter += "2";
            }
            Field idField = R.raw.class.getDeclaredField(letter);
            return idField.getInt(idField);
        } catch (Exception e) {

        }
        return R.raw.zi;
    }

    private void parseWordList(BufferedReader wordReader, String inputString) throws IOException {
        String currentLine;
        currentKey = "_INIT";
        sameKeyWordsArr = new ArrayList<String>();

        while ((currentLine = wordReader.readLine()) != null) {
            processWord(inputString, currentLine.toLowerCase().trim());
        }
        setWordMapBin(currentKey, sameKeyWordsArr);
    }

    private void processWord(String inputString, String currentWord) {
        String wordKey = currentWord.substring(0, WORD_MAP_KEY_LENGTH);

        if (matchedWordFile(inputString, wordKey)) {
            processMatchedWord(currentWord);
        }
    }

    private void processMatchedWord(String currentWord) {
        String wordKey = currentWord.substring(0, WORD_MAP_KEY_LENGTH);

        if (!firstPass(currentKey) && wordKeyHasChanged(wordKey, currentKey)) {
            setWordMapBin(currentKey, sameKeyWordsArr);
            sameKeyWordsArr = new ArrayList<String>();
        }

        // Only deal with words with minimum 3 letters
        if (currentWord.length() > 2) {
            sameKeyWordsArr.add(currentWord);
            currentKey = wordKey;
        }
    }

    private void setWordMapBin(String currentKey, ArrayList<String> sameKeyWordsArr) {
        ArrayList<String> finalArr = new ArrayList<String>();
        finalArr.addAll(sameKeyWordsArr);
        wordMap.put(currentKey, finalArr);
    }

    private boolean wordKeyHasChanged(String wordKey, String currentKey) {
        return !currentKey.equals(wordKey);
    }

    private boolean firstPass(String currentKey) {
        return currentKey.equals("_INIT");
    }

    private boolean matchedWordFile(String inputString, String wordKey) {
        return wordKey.substring(0, WORD_FILE_GRANULARITY).equals(inputString);
    }

}
