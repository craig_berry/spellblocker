package edu.neu.madcourse.craigberry2.spellblocker;

import edu.neu.mhealth.api.KeyValueAPI;

/**
 * Created by craig on 2/21/14.
 */
public class HealthServer {

    // Team Name / Password for Accessing Health Server
    static final String MHEALTH_SERVER_LOGIN = "spellblocker";
    static final String MHEALTH_SERVER_PASSWORD = "sbPw4520";

    //////////////////////////////////////////////////////
    // Server functions with credentials abstracted out
    //////////////////////////////////////////////////////

    // Check if server is available
    public static boolean isServerAvailable() {
        return KeyValueAPI.isServerAvailable();
    }

    // Store value on server with key
    public static String storeValue(String key, String value) {
        return KeyValueAPI.put(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD, key, value);
    }

    // Store value on server with key
    public static boolean storeValue(String key, String value, boolean returnType) {
        String result = KeyValueAPI.put(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD, key, value);
        return checkErrorBoolean(result);
    }

    // Does the server contain the given key
    public static boolean containsKey(String key) {
        String result = KeyValueAPI.get(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD, key);
        return checkErrorBoolean(result);
    }

    // Return the value entry for the given key
    public static String getValue(String key) {
        String result = KeyValueAPI.get(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD, key);
        return checkErrorString(result);
    }

    // Clear the server of all values
    public static boolean clearAllValues() {
        String result = KeyValueAPI.clear(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD);
        return checkErrorBoolean(result);
    }

    // Clear the key and resulting value pair from the server
    public static boolean clearKey(String key) {
        String result = KeyValueAPI.clearKey(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD, key);
        return checkErrorBoolean(result);
    }

    // Process errors on the result of a health server function that returns a boolean
    private static boolean checkErrorBoolean(String result) {
        return !result.contains("Error");
    }

    // Process errors on the result of a health server function that returns a string
    private static String checkErrorString(String result) {
        if (result.contains("Error")) {
            // Do error handling
            return "Error";
        }
        else {
            return result;
        }
    }
}
