package edu.neu.madcourse.craigberry2.spellblocker;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.neu.madcourse.craigberry2.R;

/**
 * Created by craig on 2/24/14.
 */
public class MultiplayerServerTasks extends AsyncTask<String, String, String> {

    // Data retrieval keys
     final String BATTLE_REQUEST_KEY = "edu.neu.madcourse.craigberry.spellblocker.battle.request";
     final String BATTLE_OPPONENT_KEY = "edu.neu.madcourse.craigberry.spellblocker.battle.opponent";
     final String MULTIPLAYER_ROLE_KEY = "edu.neu.madcourse.craigberry.spellblocker.mutliplayer.role";
     final String IS_AVAILABLE_KEY = "edu.neu.madcourse.craigberry.spellblocker.multiplayer.availability";
     final String BATTLE_RESPONSE_KEY = "edu.neu.madcourse.craigberry.spellblocker.battle.response";
     final String SEARCH_LIST_KEY = "edu.neu.madcourse.craigberry.spellblocker.search.list";
     final String SEARCH_LIST_LOCK_KEY = "edu.neu.madcourse.craigberry.spellblocker.search.lock";
     final String SEARCH_LOCK_ID_KEY = "edu.neu.madcourse.craigberry.spellblocker.search.lock.id";

     final String ROLE_HOST = "host";
     final String ROLE_CLIENT = "client";

     final String LOCKED = "locked";
     final String UNLOCKED = "unlocked";

     final String TRUE = "true";
     final String FALSE = "false";

     final String NULL_ID = "nullid";
     final String NULL_REQUEST = "nullrequest";

     final String REJECT_RESPONSE = "reject";
     final String ACCEPT_RESPONSE = "accept";
     final String NULL_RESPONSE = "null";
     final String CONNECT_FAILED = "connectfailed";

     final String CONNECTED = "connected";

     String ROW_DELIMETER = "\\+";
     int REFRESH_TIME = 500;
     int CONN_ATTEMPTS = 10;

//    public boolean CANCEL_FLAG = false;
    public String CANCEL_MSG = "";
    public String themeid = "";
    public Activity parentActivityContext;


    public MultiplayerServerTasks(Activity parentActivityContext, String meid) {
        if ("000000000000000".equals(meid)) {
            Random r = new Random();
            meid = r.nextInt(1000) + "";
        }
        this.themeid = meid;
        this.parentActivityContext = parentActivityContext;
    }

    public void ServerInit() {
        HealthServer.storeValue(SEARCH_LIST_KEY, "");
        HealthServer.storeValue(SEARCH_LIST_LOCK_KEY, UNLOCKED);
        HealthServer.storeValue(SEARCH_LOCK_ID_KEY, NULL_ID);
    }


    private  void startClient(String myMeid, String hostMeid) {
        boolean running = true;

        while(running) {
            if (isCancelled()) {
                break;
            }
            if (getGameRoomStatus(hostMeid, myMeid).equals(TRUE)) {
                running = false;
            }
        }
        // Check ensures we exited 'while' as expected, not via cancel
        if (!running) {
            setConnectedTo(myMeid, hostMeid);
            publishProgress("Game started, Client(Me):" + myMeid + ":Host:" + hostMeid);
            // Play game
            playGame(ROLE_CLIENT, myMeid, hostMeid);
        }
    }

     final String CONNECTED_TO_KEY = "edu.neu.madcourse.craigberry.spellblocker.connectedto";
     final String GAME_ROOM_KEY = "edu.neu.madcourse.craidberry.spellblocker.gameroom";

    private  void startHost(String myMeid, String clientMeid) {
        boolean running = true;
        setGameRoomStatus(myMeid, clientMeid, TRUE);

        while(running) {
            if (isCancelled()) {
                running = false;
            }
            if (getConnectedTo(clientMeid).equals(myMeid)) {
                setConnectedTo(myMeid, clientMeid);

                // Play game
                publishProgress("Game started, Client:" + clientMeid + ":Host(Me):" + myMeid);
                playGame(ROLE_HOST, myMeid, clientMeid);
            }
        }
    }

    private  void playGame(String role, String myMeid, String theirMeid) {
        boolean running = true;

        while(!isCancelled() && running) {
            if (!getConnectedTo(theirMeid).equals(myMeid)) {
                publishProgress("Connection Lost...");
                running = false;
                int attempts = 10;
                while (attempts > 0) {
                    publishProgress("Reconnect attempt " + (10 - attempts));
                    if (getConnectedTo(theirMeid).equals(myMeid)) {
                        publishProgress("Connection restablished, (Me):" + myMeid + ":(Them):" + theirMeid);
                        running = true;
                        break;
                    } else {
                        attempts--;
                        try {
                            Thread.sleep(REFRESH_TIME);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                // GAME LOGIC
            }
        }
        if (role == ROLE_HOST) {
            setGameRoomStatus(myMeid, theirMeid, FALSE);
        }
        publishProgress("Game Ended");
    }

    private  void init(String meid) {
        repairServer();
        setBattleRequest(meid, NULL_REQUEST);
        setBattleResponse(meid, "No Response Set");
        setAvailable(meid, TRUE);
        setConnectedTo(meid, NULL_ID);
        REFRESH_TIME = 500;
        CONN_ATTEMPTS = 10;
    }

    private void repairServer() {
        //ServerInit();
        if (!getSearchListLock().equals(UNLOCKED) && !getSearchListLock().equals(LOCKED)) {
//            publishProgress("Lock repaired");
            unlockSearchList();
        }
        String gameList = getSearchList();
        if (gameList.contains("Error")) {
//            publishProgress("Search List repaired/cleared");
            setSearchList("");
        }

        gameList = getSearchList();
        if (gameList.length() > 0) {
            if (gameList.substring(0,1).equals("+")) {
//                publishProgress("Invalid First Char: " + gameList);
                gameList = gameList.substring(1);
                setSearchList(gameList);
//                publishProgress("New List: " + getSearchList());
            }
        }

        gameList = getSearchList();
        Pattern p = Pattern.compile("\\+\\++");
        Matcher m = p.matcher(gameList);
        if (m.find()) {
//            publishProgress("Invalid List: " + gameList);
            String list = "";
            lockSearchList(themeid);
            String[] searchList = getSearchList().split(ROW_DELIMETER);
            for (int i = 0; i < searchList.length; i++) {
                if (!searchList[i].equals("")) {
                    if (i > 0) {
                        list += ROW_DELIMETER + searchList[i];
                    } else {
                        list += searchList[i];
                    }
                }
            }
            setSearchList(list);
//            publishProgress("New List: " + getSearchList());
            unlockSearchList();
        }
        m = null;

        if (getSearchLockId().contains("Error")) {
//            publishProgress("Lock ID repaired");
            setSearchLockId("");
        }
    }

    private  String waitForOpponent(String myMeid) {

        // Prepare to wait
        lockSearchList(themeid);
        addToSearchList(themeid);
//        publishProgress("Add self to list");
        unlockSearchList();
        setBattleResponse(themeid, NULL_RESPONSE);
        publishProgress("Waiting for contact...");

        boolean running = true;

        while(running) {
            if (isCancelled()) {
                break;
            }

            String req = getBattleRequest(myMeid);
            if (!req.equals(NULL_REQUEST)) {
                //*** TO ADD: PROMPT WHETHER TO CONNECT OR NOT
//                publishProgress("Sending response:" + getAcceptResponse(myMeid));
                setBattleResponse(req, getAcceptResponse(myMeid));
                return req;
            }

        }
        cancel(true);
        return "cancel";
    }

    private  String getAcceptResponse(String meid) {
        return ACCEPT_RESPONSE + meid;
    }

    private  String getRejectResponse(String meid) {
        return REJECT_RESPONSE + meid;
    }

    private  String getNullResponse() {
        return NULL_RESPONSE;
    }
    private  void unlockSearchList() {
        setSearchListLock(UNLOCKED);
        setSearchLockId(NULL_ID);
    }


    private  void addToSearchList(String meid) {
        if (isLockMine(meid)) {
            String searchList = getSearchList();
            searchList += ROW_DELIMETER + meid;
            setSearchList(searchList);
        } else {
            cancel("addToSearchList:" + meid, "Lock does not belong to user");
        }
    }

    private  String attemptConnections(String myMeid, String[] searchArr) {
        String theirMeid = "";

        for (int i = 0; i < searchArr.length; i++) {
            if (isCancelled()) {
                break;
            }

            theirMeid = searchArr[i];

            if(requestBattleGame(myMeid, theirMeid)) {
                if (battleAccepted(myMeid, theirMeid)) {
                    return theirMeid;
                }
            }
        }
        return CONNECT_FAILED;
    }

    private  boolean battleAccepted(String myMeid, String theirMeid) {
        boolean running = true;
        int counter = 10;

        while(running) {
            if (isCancelled()) {
                break;
            }
            if (counter > 0) {
                String response = getBattleResponse(myMeid);
                if (getRejectResponse(theirMeid).equals(response)) {
                    return false;
                } else if (getAcceptResponse(theirMeid).equals(response)) {
                    return true;
                }
                counter--;
                // WAIT
//                publishProgress("Response: " + theirMeid + " : " + response);
                try {
                    Thread.sleep(REFRESH_TIME);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                running = false;
            }
        }
        return false;
    }

    private  boolean requestBattleGame(String myMeid, String theirMeid) {
//        publishProgress("Request: " + theirMeid);
        if (isAvailable(theirMeid)) {
            setAvailable(theirMeid, FALSE);
            setBattleRequest(theirMeid, myMeid);
            return true;
        }
//        publishProgress("Response: " + "Not available");
        return false;
    }

    private  boolean isSearchListLocked() {
        return getSearchListLock().equals(LOCKED);
    }

    private  boolean isLockMine(String meid) {
        return getSearchLockId().equals(meid);
    }


    private  boolean isAvailable(String theirMeid) {
        return (TRUE).equals(getData(getAvailableKey(theirMeid), "GetIsAvailable:" + theirMeid, false));
    }


    private boolean lockSearchList(String meid) {
        int attempts = 10;

        try {
            while (attempts > 0) {
                if (isCancelled()) {
                    break;
                }
                String lock = getSearchListLock();

                // If someone else doesn't have the lock we take it
                if (lock.equals(UNLOCKED)) {
                    setSearchListLock(LOCKED);
                    setSearchLockId(meid);
                    return true;
                }
                // Otherwise wait, and try again
                else {
                    Thread.sleep(REFRESH_TIME);
                }
                attempts--;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return false;
    }

    private  void cancel(String errorKey, String errorStr) {
        cancel(true);
        CANCEL_MSG = "Function: " + errorKey + " | Return: " + errorStr;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///////// GET DATA
    ///////////////////////////////////////////////////////////////////////////////////

    private  String getAvailableKey(String meid) {
        return IS_AVAILABLE_KEY + meid;
    }

    private  String getBattleRequestKey(String meid) {
        return BATTLE_REQUEST_KEY + meid;
    }

    private  String getBattleResponseKey(String meid) {
        return BATTLE_RESPONSE_KEY + meid;
    }

    public  String getConnectedToKey(String meid) {
        return CONNECTED_TO_KEY + meid;
    }

    public  String getGameRoomKey(String hostMeid, String clientMeid) {
        return GAME_ROOM_KEY + ":" + hostMeid + ":" + clientMeid;
    }

    public  String getConnectedTo(String meid) {
        return getData(getConnectedToKey(meid), "GetConnectedTo:" + meid, true);
    }

    public  String getGameRoomStatus(String hostMeid, String clientMeid) {
        return getData(getGameRoomKey(hostMeid, clientMeid), "GetGameRoom:H:" + hostMeid + ":C:" + clientMeid, false);
    }

    private  String getBattleRequest(String meid) {
        return getData(getBattleRequestKey(meid), "GetBattleRequest:" + meid, true);
    }

    private  String getBattleResponse(String meid) {
        return getData(getBattleResponseKey(meid), "GetBattleResponse:" + meid, true);
    }

    private  String getSearchList() {
        return getData(SEARCH_LIST_KEY, "GetSearchList", true);
    }

    private  String getSearchLockId() {
        return getData(SEARCH_LOCK_ID_KEY, "GetSearchLockId", true);
    }

    private  String getSearchListLock() {
        return getData(SEARCH_LIST_LOCK_KEY, "GetSearchListLock", true);
    }

    private  String getData(String key, String errorKey, boolean cancelOnError) {
        String returnStr = HealthServer.getValue(key);

        if (returnStr.contains("Error") && cancelOnError) {
            cancel(errorKey, returnStr);
        }

        return returnStr;
    }

    ///////////////////////////////////////////////////////////////////////////////////
    ///////// SET DATA
    ///////////////////////////////////////////////////////////////////////////////////

    public  String setConnectedTo(String meid, String value) {
        return setData(getConnectedToKey(meid), value, "SetConnectedTo:Key:" + meid + "| val:" + value);
    }

    public  String setGameRoomStatus(String hostMeid, String clientMeid, String status) {
        return setData(getGameRoomKey(hostMeid, clientMeid), status, "SetGameRoom:Key:" + hostMeid + "| val:" + status);
    }

    private  void setAvailable(String theirMeid, String available) {
        setData(getAvailableKey(theirMeid), available, "SetAvailabile:" + theirMeid);
    }

    private  void setBattleRequest(String theirMeid, String myMeid) {
        setData(getBattleRequestKey(theirMeid), myMeid, "SetBattleRequest:key=" + theirMeid + ":val=" + myMeid);
    }

    private  void setBattleResponse(String theirMeid, String response) {
        setData(getBattleResponseKey(theirMeid), response, "SetBattleResponse:key=" + theirMeid + ":val=" + response);
    }

    private  void setSearchLockId(String meid) {
        setData(SEARCH_LOCK_ID_KEY, meid, "SetSearchLockId");
    }

    private  void setSearchListLock(String lockValue) {
        setData(SEARCH_LIST_LOCK_KEY, lockValue, "SetSearchListLock");
    }

    private  void setSearchList(String searchList) {
        setData(SEARCH_LIST_KEY, searchList, "SetSearchList");
    }

    private  String setData(String key, String value, String errorKey) {

        String returnStr = HealthServer.storeValue(key, value);

        if (returnStr.contains("Error")) {
            cancel(errorKey, returnStr);
        }

        return returnStr;
    }

    /// Async Task Functions
    protected String doInBackground(String... s) {
        String answerId = CONNECT_FAILED;
        init(themeid);

        if (HealthServer.isServerAvailable()) {
            if (!isCancelled()) {
                String searchList = getSearchList();
//                publishProgress("Game Queue: " + searchList);
                String[] searchArr = getSearchList().split(ROW_DELIMETER);

                if (searchList.matches("\\+*")) {
                    publishProgress("No users found online");
                    answerId = CONNECT_FAILED;
                } else {
                    publishProgress("Contacting users in game queue");
                    answerId = attemptConnections(themeid, searchArr);
                }
            }

            if (!isCancelled()) {
                if (!answerId.equals(CONNECT_FAILED)) {
                    // START GAME !!!!!
                    publishProgress("Game Found! ID:" + answerId);
                    startHost(themeid, answerId);
                } else {
                    publishProgress("No games found");
                    publishProgress("Switching from 'Search' to 'Wait'");
                    // If answer not returned, add self to searching list and enter wait loop
                    // Acquire Lock on data before editing
                    if (!isCancelled()) {

                        String opponent = waitForOpponent(themeid);
                        if (!opponent.equals("cancel")) {
                            publishProgress("Game found! ID:" + opponent);
                            startClient(themeid, opponent);
                        }
                    }
                }
            }
        } else {
            publishProgress("Server unavailable");
        }

        // *Log off* player from multiplayer
        setConnectedTo(themeid, NULL_ID);
        removeFromSearchList(themeid);

        if (isCancelled()) {
            publishProgress("Canceled: " + CANCEL_MSG);
        }

        return "";
    }

    protected void onProgressUpdate(String... progress) {
        TextView t = (TextView) parentActivityContext.findViewById(R.id.comm_info_text);
        if (progress.length > 0) {
            t.setText(t.getText() + "\n" + progress[0]);
        }
        final ScrollView scrollView = (ScrollView) parentActivityContext.findViewById(R.id.comm_scrollview);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    protected void onPostExecute(String result) {

    }

    private void removeFromSearchList(String meid) {
        String newList = "";
        if (HealthServer.isServerAvailable()) {
            lockSearchList(meid);
            String[] arr = getSearchList().split(ROW_DELIMETER);

            for (int i = 0; i < arr.length; i++) {
                if (!arr[i].equals(meid)) {
                    newList += ROW_DELIMETER + arr[i];
                }
            }
            setSearchList(newList);
            unlockSearchList();
        }
    }


}
