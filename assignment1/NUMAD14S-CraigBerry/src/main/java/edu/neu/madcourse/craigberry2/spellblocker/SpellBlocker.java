package edu.neu.madcourse.craigberry2.spellblocker;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import edu.neu.madcourse.craigberry2.R;

public class SpellBlocker extends ActionBarActivity implements View.OnClickListener {
    final String PREF_KEY = "edu.neu.madcourse.craigberry.spellblocker.prefs";
    final String PREF_CONT = "edu.neu.madcourse.craigberry.spellblocker.continue";
    SharedPreferences prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.spellblocker_main);
        setTitle("Word Game");
        prefs = this.getSharedPreferences(
                PREF_KEY, Context.MODE_PRIVATE);

        // Set up click listeners for all the buttons
        View continueButton = findViewById(R.id.wordgame_instructions_button);
        continueButton.setOnClickListener(this);
        View newButton = findViewById(R.id.wordgame_new_button);
        newButton.setOnClickListener(this);
        View aboutButton = findViewById(R.id.wordgame_ack_button);
        aboutButton.setOnClickListener(this);
        View exitButton = findViewById(R.id.wordgame_exit_button);
        exitButton.setOnClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Music.play(this, R.raw.sb_main);
    }

    @Override
    protected void onPause() {
        super.onPause();

        Music.stop();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.wordgame_new_button:
                openNewGameDialog();
                break;
            case R.id.wordgame_instructions_button:
                Intent instructions = new Intent(this, Instructions.class);
                startActivity(instructions);
                break;
            case R.id.wordgame_ack_button:
                Intent ack = new Intent(this, Acknowledgements.class);
                startActivity(ack);
                break;
            case R.id.wordgame_exit_button:
                finish();
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.spellblocker_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sbSettings:
                startActivity(new Intent(this, Prefs.class));
                return true;
            // More items go here (if any) ...
        }
        return false;
    }

    /**
     * Start a new game with the given difficulty level
     */
    private void startGame(int diff) {
        Log.d("Word Game", "Leggo");
        Intent intent = new Intent(this, Game.class);
        intent.putExtra(Game.DIFF_EXTRA, diff);
        startActivity(intent);
    }

    /** Ask the user what difficulty level they want */
    private void openNewGameDialog() {
        String[] arr;
        final boolean cont = prefs.getBoolean(PREF_CONT, false);
        if (cont) {
            arr = new String[4];
            arr[0] = "Continue Game";
            arr[1] = "New: Easy";
            arr[2] = "New: Medium";
            arr[3] = "New: Hard";
        }
        else {
            arr = new String[3];
            arr[0] = "New: Easy";
            arr[1] = "New: Medium";
            arr[2] = "New: Hard";
        }

        new AlertDialog.Builder(this)
                .setTitle(R.string.new_game_title)
                .setItems(arr,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialoginterface, int i) {
                                if (cont) {
                                    startGame(i - 1);
                                }
                                else {
                                    startGame(i);
                                }
                            }
                        })
                .show();
    }
}
