package edu.neu.madcourse.craigberry2.spellblocker_multiplayer;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import edu.neu.madcourse.craigberry2.R;

public class ChallengePrompt extends Activity {

    // Intent keys
    public static final String CONTENDER_MEID =
            "edu.neu.madcourse.craigberry.spellblocker.contender_meid";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupActivity();
        setupPrompt();
    }

    private void setupActivity() {
        setContentView(R.layout.challenge_prompt);
        setTitle("Challenge Found");
    }

    private void setupPrompt() {
        // Get opponent meid
        String contenderMeid = this.getIntent().getStringExtra(CONTENDER_MEID);
        TextView opponentString = (TextView) findViewById(R.id.prompt_opponent_text);
        opponentString.setText(opponentString.getText() + getOpponentString(contenderMeid));
    }

    private String getOpponentString(String contenderMeid) {
        // TODO: Add wins, loss, other revelant info
        return contenderMeid;
    }


    public void acceptButtonClick(View view) {
        setResult(ChallengeMode.ACCEPT_RESULT);
        finish();
    }

    public void rejectButtonClick(View view) {
        setResult(ChallengeMode.REJECT_RESULT);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.challenge_prompt, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
