package edu.neu.madcourse.craigberry2.spellblocker;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import edu.neu.madcourse.craigberry2.R;

public class Acknowledgements extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Acknowledgements ");
        setContentView(R.layout.spellblocker_acknowledgements);
    }
}
