package edu.neu.madcourse.craigberry2.communications;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import edu.neu.madcourse.craigberry2.R;
import edu.neu.madcourse.craigberry2.spellblocker.Game;
import edu.neu.madcourse.craigberry2.spellblocker.HealthServer;
import edu.neu.madcourse.craigberry2.spellblocker.HighScore;
import edu.neu.madcourse.craigberry2.spellblocker.MultiplayerServerTasks;
import edu.neu.madcourse.craigberry2.spellblocker.ScoreTracker;
import edu.neu.mhealth.api.KeyValueAPI;

public class Communications extends ActionBarActivity {
    final String TEST_KEY = "edu.neu.madcourse.craigberry.communications.value";

    // Constant's to represent actions
    static final int ACTION_SERVER_AVAILABILITY = 2198432;
    static final int ACTION_FIND_PLAYER = 5292317;
    static final int ACTION_INCREMENT = 7437292;
    static final int ACTION_RESET = 5524987;
    static final int ACTION_READ = 3421287;
    static final int ACTION_HIGH_SCORES = 5524986;
    static final int ACTION_CLEAR_ALL = 2141253;

    // Team Name / Password for Accessing Health Server
    static final String MHEALTH_SERVER_LOGIN = "spellblocker";
    static final String MHEALTH_SERVER_PASSWORD = "sbPw4520";

    // Vars
    int count = 0;
    MultiplayerServerTasks multiplayerTask;
    ServerTest serverTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.communications);
        setSpinnerAdapter();

        multiplayerTask = new MultiplayerServerTasks(this, getMEID());
        serverTask = new ServerTest();

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .commit();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);

        Spinner spinner = (Spinner) findViewById(R.id.comm_actions_spinner);
        savedInstanceState.putInt("SelectedAction", spinner.getSelectedItemPosition());
        TextView commText = (TextView) findViewById(R.id.comm_info_text);
        savedInstanceState.putString("CommTextbox", commText.getText().toString());
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        int myInt = savedInstanceState.getInt("SelectedAction");
        String myString = savedInstanceState.getString("CommTextbox");
        setText(myString);
        Spinner spinner = (Spinner) findViewById(R.id.comm_actions_spinner);
        spinner.setSelection(myInt);
        moveScrollView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.communications, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getSelectedAction()==ACTION_FIND_PLAYER) {
            multiplayerTask.cancel(true);

        } else {
            serverTask.cancel(true);
        }
    }

    // Server functions with credentials abstracted out
    private static boolean isServerAvailable() {
        return KeyValueAPI.isServerAvailable();
    }

    private static String storeValue(String key, String value) {
        return KeyValueAPI.put(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD, key, value);
    }

    private static String getValue(String key) {
        return KeyValueAPI.get(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD, key);
    }

    private static String clearAllValues() {
        return KeyValueAPI.clear(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD);
    }

    private static String clearKey(String key) {
        return KeyValueAPI.clearKey(MHEALTH_SERVER_LOGIN, MHEALTH_SERVER_PASSWORD, key);
    }

    // Spinner functions
    private int getSelectedAction() {
        Spinner spinner = (Spinner) findViewById(R.id.comm_actions_spinner);
        String selection = (String)spinner.getSelectedItem();

        if (selection.equals(getString(R.string.server_avail))) {
            return ACTION_SERVER_AVAILABILITY;
        }
        if (selection.equals(getString(R.string.find_player))) {
            return ACTION_FIND_PLAYER;
        }
        if (selection.equals(getString(R.string.increment_value))) {
            return ACTION_INCREMENT;
        }
        if (selection.equals(getString(R.string.read_value))) {
            return ACTION_READ;
        }
        if (selection.equals(getString(R.string.reset_value))) {
            return ACTION_RESET;
        }
        if (selection.equals(getString(R.string.high_score))) {
            return ACTION_HIGH_SCORES;
        }
        if (selection.equals(getString(R.string.clear_all))) {
            return ACTION_CLEAR_ALL;
        }
        return -1;
    }

    private void setSpinnerAdapter() {
        Spinner spinner = (Spinner) findViewById(R.id.comm_actions_spinner);
        // Create an ArrayAdapter using default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.comm_actions_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    // UI Functions
    public void moveScrollView() {
        final ScrollView scrollView = (ScrollView) findViewById(R.id.comm_scrollview);
        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.fullScroll(ScrollView.FOCUS_DOWN);
            }
        });
    }

    // Text Box Functions
    private void clearText() {
        TextView commText = (TextView) findViewById(R.id.comm_info_text);
        commText.setText("");
        count = 0;
    }

    private void setText(String text) {
        TextView commText = (TextView) findViewById(R.id.comm_info_text);
        commText.setText(text);
    }

    private void addText(String text) {
        TextView commText = (TextView) findViewById(R.id.comm_info_text);
        commText.setText(commText.getText() + "\n" + text);
        moveScrollView();
    }

    // Button Clicks
    public void runCommunicationTest(View view) {
        int action = getSelectedAction();
        if (action == ACTION_FIND_PLAYER) {
            if (multiplayerTask.getStatus() != AsyncTask.Status.RUNNING) {
                multiplayerTask = new MultiplayerServerTasks(this, getMEID());
                multiplayerTask.execute();
            } else {
                addText("Task already running");
            }
        } else {
            if (serverTask.getStatus() != AsyncTask.Status.RUNNING) {
                serverTask = new ServerTest();
                serverTask.execute(action);
            } else {
                addText("Task already running");
            }
        }
    }

    public void commStopBtn(View view) {
        stopTasks();
    }

    public void communicationClearButton(View view) {
        clearText();
    }

    public void communicationExitButton(View view) {
        stopTasks();
        finish();
    }

    public void commAckBtn(View view) {
        Intent ackIntent = new Intent(getBaseContext(), CommAcknowledgements.class);
        startActivity(ackIntent);
    }

    private void stopTasks() {
        if (getSelectedAction()==ACTION_FIND_PLAYER) {
            multiplayerTask.cancel(true);
            addText("Stopped by user");
        } else {
            if (serverTask.getStatus()== AsyncTask.Status.RUNNING) {
                serverTask.cancel(true);
                addText("Stopped by user");
            }
        }
    }

    private String getMEID() {
        TelephonyManager telephonyManager = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    // Async Tasks for Web calls

    public class ServerTest extends AsyncTask<Integer, Integer, String> {

        @Override
        protected String doInBackground(Integer... params) {
            int count = params.length;

            for (int i=0; i < count; i++) {
                switch(params[i]) {
                    case ACTION_SERVER_AVAILABILITY: return runServerAvailabilityTest();
                    case ACTION_FIND_PLAYER:
                        break;
                    case ACTION_INCREMENT: return increment();
                    case ACTION_READ: return read();
                    case ACTION_RESET: return reset();
                    case ACTION_HIGH_SCORES: return displayHighScores();
                    case ACTION_CLEAR_ALL:
                        HealthServer.clearAllValues();
                        return "All server values cleared";
                    default: break;
                }
            }
            return "Nothing to return";
        }

        @Override
        protected void onPostExecute(String result) {
            addText(result);
        }

        private String runServerAvailabilityTest() {
            String result = "Pinging health server...";
            if (isServerAvailable()) {
                result += "\n" + "Server is available";
            }
            else {
                result += "\n" + "Server is not available";
            }
            return result + "\n";
        }

        private String writeDataTest() {
            if (isServerAvailable()) {
                // Setup key/val pairs
                String result = "", key1 = "key1", key2 = "key2", key3 = "key3",
                        val1 = "val1", val2 = "val2", val3 = "val3";

                // Write data
                result += "Storing value: '" + val1 + "' with key: '" + key1 + "'" + "\n";
                if (storeValue(key1, val1).equals("true")) {
                    result += "Success";
                }
                else {
                    result += "Failed";
                }
                result += "\n";

                result += "Storing value: '" + val2 + "' with key: '" + key2 + "'" + "\n";
                if (storeValue(key1, val1).equals("true")) {
                    result += "Success";
                }
                else {
                    result += "Failed";
                }
                result += "\n";

                result += "Storing value: '" + val3 + "' with key: '" + key3 + "'" + "\n";
                if (storeValue(key1, val1).equals("true")) {
                    result += "Success";
                }
                else {
                    result += "Failed";
                }
                result += "\n";

                // Return results
                return result;
            }
            else {
                return "Failed: Server is unavailable" + "\n";
            }
        }

        private String readDataTest() {
            if (isServerAvailable()) {
                // Setup key/val pairs
                String result = "", key1 = "key1", key2 = "key2", key3 = "key3",
                        val1 = "val1", val2 = "val2", val3 = "val3";

                // Write data
                result += "Storing value: '" + val1 + "' with key: '" + key1 + "'" + "\n";
                storeValue(key1, val1);
                result += "Storing value: '" + val2 + "' with key: '" + key2 + "'" + "\n";
                storeValue(key2, val2);
                result += "Storing value: '" + val3 + "' with key: '" + key3 + "'" + "\n";
                storeValue(key3, val3);

                // Read data
                result += "Reading key:'" + key1 + "' returns:'" + getValue(key1) + "'" + "\n";
                result += "Reading key:'" + key2 + "' returns:'" + getValue(key2) + "'" + "\n";
                result += "Reading key:'" + key3 + "' returns:'" + getValue(key3) + "'" + "\n";

                return result;
            }
            else {
                return "Failed: Server is unavailable" + "\n";
            }
        }

        private String displayHighScores() {
            String result = "";
            if (isServerAvailable()) {
                String scoreList = ScoreTracker.getSerializedScoreList(Game.DIFFICULTY_EASY);
                if (!scoreList.contains("Error")) {
                    ArrayList<HighScore> scores = ScoreTracker.buildHighScoreList(scoreList);
                    result += "Rank" + " : " + "Name     " + " : " + "Score  " + " : " + "Time" + " : " + "Date        " + "\n";

                    for (int i = 0; i < scores.size(); i++) {
                        HighScore h = scores.get(i);
                        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd");
                        try {
                            Date date = h.getDatetime().getTime();
                            int iCol = 4 - (h.getIndex() + "").length(),
                                nameCol = 9 - h.getName().length(),
                                scrCol = 7 - (h.getScore() + "").length(),
                                timeCol = 4 - (h.getTime() + "").length();

                            result += h.getIndex() + addSpaces(iCol) + " : " + h.getName() + addSpaces(nameCol) + " : " + h.getScore() + addSpaces(scrCol) + " : " + h.getTime() + addSpaces(timeCol) + " : " + dateFormat.format(date) + "\n";

                        } catch (Exception e){

                        }
                    }
                    return result;
                }
                return "No scores entered";
            }
            return "Server is unavailable";
        }

        private String addSpaces(int num) {
            String s = "";
            for (int i = 0; i < num; i++) {
                s += " ";
            }
            return s;
        }
    }

    private String increment() {
        if (HealthServer.isServerAvailable()) {
            String result = "";
            int val = Integer.parseInt(getTheValue());
            val++;
            storeValue(TEST_KEY, val+"");
            result += "Now: " + val;
            return result;
        } else {
            return "Server unavailable";
        }
    }

    private String read() {
        if (isServerAvailable()) {
            return "Val: " + getTheValue();
        } else {
            return "Server unavailable";
        }
    }

    private String getTheValue() {
        if (HealthServer.isServerAvailable()) {
            if (getValue(TEST_KEY).contains("Error")) {
                storeValue(TEST_KEY, 0 + "");
                return "0";
            }
            return getValue(TEST_KEY);
        } else {
            return "Server unavailable";
        }
    }

    private String reset() {
        if (HealthServer.isServerAvailable()) {
            String result = "";
            storeValue(TEST_KEY, 0 + "");
            result += "Set: " + 0;
            return result;
        } else {
            return "Server unavailable";
        }
    }



}
