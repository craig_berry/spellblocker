package edu.neu.madcourse.craigberry2.spellblocker_multiplayer;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import edu.neu.madcourse.craigberry2.R;
import edu.neu.madcourse.craigberry2.spellblocker.Game;

public class ChallengeMode extends ActionBarActivity {

    // Data variables
    private int EASY_DIFFICULTY = 0;
    private String OPPONENT_MEID = "";
    private Challenge challenge;

    // Result and Request Codes
    public static int ACCEPT_RESULT = 12;
    public static int REJECT_RESULT = 23;
    public static int PROMPT_REQUEST = 34;
    public static int GAME_REQUEST = 45;
    public static int GAME_WON_RESULT = 56;
    public static int GAME_LOST_RESULT = 67;

    // Intent keys
    public static final String IS_CHALLENGE_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.challengegame.ischallenge";
    public static final String WORD_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.challengegame.words";
    public static final String SCORE_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.challengegame.score";
    public static final String TIME_EXTRA =
            "edu.neu.madcourse.craigberry.spellblocker.challengegame.time";

    // Constants
    public static String OPPONENT_DELIMETER = ",";
    public static String CHALLENGE_DELIMETER = "/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.challenge);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.challenge, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Return phone user id
    private String getMEID() {
        TelephonyManager telephonyManager =
                (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        return telephonyManager.getDeviceId();
    }

    // Initiate a New Challenge
    public void findNewChallenge(View view) {
        OPPONENT_MEID = getRandomUser();
        promptChallenge(OPPONENT_MEID);
    }

    // Setup challenge object given opponent meid after accepting prompt
    private void setupChallenge(String opponentMeid) {
        challenge = new Challenge(new Opponent(getMEID()), new Opponent(opponentMeid));
        startChallenge(challenge);
    }

    // Starts the actual game
    private void startChallenge(Challenge challenge) {
        Log.d("Two Player", "Start Challenge against: " + challenge.getContender().getMeid());
        Intent gameIntent = new Intent(this, Game.class);
        gameIntent.putExtra(Game.DIFF_EXTRA, EASY_DIFFICULTY);
        gameIntent.putExtra(IS_CHALLENGE_EXTRA, true);
        startActivityForResult(gameIntent, GAME_REQUEST);
    }

    // Result of prompt handled in onActivityResult
    private void promptChallenge(String opponentMeid) {
        Intent challengePromptIntent = new Intent(getBaseContext(), ChallengePrompt.class);
        challengePromptIntent.putExtra(ChallengePrompt.CONTENDER_MEID, opponentMeid);
        startActivityForResult(challengePromptIntent, PROMPT_REQUEST);
    }

    // Handles result of ChallengePrompt
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("Challenge Game Result", resultCode + "");
        // Handles Prompt
        if (requestCode == PROMPT_REQUEST) {
            if (resultCode == REJECT_RESULT) {
                //finish();
            }
            else if (resultCode == ACCEPT_RESULT && !OPPONENT_MEID.equals("")) {
                setupChallenge(OPPONENT_MEID);
            }
        }
        else if (requestCode == GAME_REQUEST) {
            if (resultCode == GAME_WON_RESULT) {
                int score = data.getIntExtra(SCORE_EXTRA, -1),
                    time = data.getIntExtra(TIME_EXTRA, -1),
                    words = data.getIntExtra(WORD_EXTRA, -1);
                processWin(score, time, words);
            }
            else if (resultCode == GAME_LOST_RESULT) {
                processLoss();
            }

        }
    }

    private void processWin(int score, int time, int words) {
        Log.d("Challenge Won", "Score: " + score + ", Time: " + time + ", Words: " + words);
        challenge.getChallenger().setHasPlayed(true);
    }

    private void processLoss() {
        Log.d("Challenge Lost", "Loss");
        challenge.getChallenger().setHasPlayed(true);
    }

    // Return random meid to challenge
    private String getRandomUser() {
        // TODO: Implement search for random users
        return "4798235692834";
    }

    // Handle Button Clicks
    public void clickBackButton(View view) {
        finish();
        System.exit(0);
    }
}
